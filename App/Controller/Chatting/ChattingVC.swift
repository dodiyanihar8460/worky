//
//  ChattingVC.swift
//  HIITList
//
//  Created by Bhavesh Patel on 28/10/17.
//  Copyright © 2018 Birger LLC. All rights reserved.
//

import UIKit
import Photos
import FirebaseAuth
import Firebase
import FirebaseDatabase
import FirebaseStorage
import ISEmojiView
import RNCryptor
import UITextView_Placeholder
import RxSwift
import ObjectMapper
import NSDate_Time_Ago
import DKImagePickerController

class ChattingVC: UIViewController, UIGestureRecognizerDelegate, UITextViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var requestView: UIView!
    @IBOutlet weak var requestTitle: UILabel!
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var viewMsg: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var consviewMessageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var userCurrentStatus: UILabel!
    @IBOutlet weak var constraintHeaderHeight: NSLayoutConstraint!
    override var canBecomeFirstResponder: Bool{
        return true
    }
    
    let locationManager = CLLocationManager()
    var items = [Message]()
    let imagePicker = UIImagePickerController()
    let barHeight: CGFloat = 50
    var showUnreadMessage = true
    var currentUser: User? {
        didSet {
            // self.title = currentUser?.name
            
            userDefaults.set(currentUser?.name, forKey: keyIsChattingwith)
            userDefaults.set(currentUser?.id, forKey: keyCurrentChatUserId)
            //            self.getLoginUsername()
            //            self.backGroundRequestForGetReceiverToken()
            
            if let currentUserID = Auth.auth().currentUser?.uid {
                Database.database().reference().child("users").child(currentUserID).child("conversations").child(currentUser!.id!).observe(.value, with: { (snapshot) in
                    if snapshot.exists() {
                        let data = snapshot.value as! [String: Any]
                        if let location = data["location"] as? String {
                        
                        Database.database().reference().child("conversations").child(location).observe(.childChanged, with: { (snapshot) in
                            
                            if let index = self.items.firstIndex(where: {$0.messageId == snapshot.key}) {
                                if let defaults = (snapshot.value as! NSDictionary)["isRead"] as? Bool {
                                    DispatchQueue.main.async {
                                        self.items[index].isRead = defaults
                                        self.tblView.reloadRows(at: [IndexPath(item: index, section: 0)], with: .none)
                                    }
                                }
                            }
                        })
                        
                        Database.database().reference().child("conversations").child(location).observe(.childChanged, with: { (snapshot) in
                            
                            if let index = self.items.firstIndex(where: {$0.messageId == snapshot.key}) {
                                if let defaults = (snapshot.value as! NSDictionary)["isReceived"] as? Bool {
                                    DispatchQueue.main.async {
                                        self.items[index].isReceived = defaults
                                        self.tblView.reloadRows(at: [IndexPath(item: index, section: 0)], with: .none)
                                    }
                                }
                            }
                        })
                        
                        Database.database().reference().child("conversations").child(location).observe(.childRemoved, with: { (snapshot) in
                            
                            if let index = self.items.firstIndex(where: {$0.messageId == snapshot.key}) {
                                self.items.remove(at: index)
                                self.tblView.deleteRows(at: [IndexPath(item: index, section: 0)], with: .fade)
                            }
                        })
                        
                    }
                    }
                })
            }
        }
    }
    
    var canSendLocation = true
    
    var ref: DatabaseReference!
    
    var senderUserName = ""
    var fcmReceiverToken = ""
    
    var messageReadObserver : UInt?
    
    var disposeBag = DisposeBag()
    
    func customization() {
        //KeyboardAvoiding.avoidingView = self.view
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if Utills.instance.isIphoneX(){
            
            bottomConstraint.constant = 20
            self.constraintHeaderHeight.constant = 80
            
        }else{
            
            bottomConstraint.constant = 0
            self.constraintHeaderHeight.constant = 70
        }
        
        self.lblTitle.text = currentUser?.name
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.width/2
        
        self.imgProfile.image = UIImage(named: "ic_defaultProfile_white")
        self.imgProfile.sd_setShowActivityIndicatorView(true)
        self.imgProfile.sd_setIndicatorStyle(.gray)
        self.imgProfile.sd_setImage(with: URL(string: self.currentUser?.profilePic ?? ""), completed: { (image, error, cacheType, url) in
            if (error != nil) {
                self.imgProfile.image = UIImage(named: "ic_defaultProfile_white")
            }
        })
        
//        if let img = currentUser?.profilePic{
//
//            if img == UIImage(named: "ic_defaultProfile"){
//
//                self.imgProfile.image = UIImage(named: "ic_defaultProfile_white")
//            }else{
//
//                self.imgProfile.image = img
//            }
//        }
        
        viewMsg.layer.cornerRadius = viewMsg.frame.height/2
        viewMsg.layer.masksToBounds =  true
        viewMsg.layer.borderColor = UIColor.lightGray.cgColor
        viewMsg.layer.borderWidth = 1.0
        txtMessage.placeholder = "Type your message here..."
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGestureDown))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.viewMessage.addGestureRecognizer(swipeDown)
        
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressChatMsg(_:)))
        longPressGesture.minimumPressDuration = 0.3
        self.tblView.addGestureRecognizer(longPressGesture)
        
        tblView.register(UINib(nibName: "ChatCell", bundle: nil), forCellReuseIdentifier: "idCellChat")
        tblView.register(UINib(nibName: "ChatCellRecieved", bundle: nil), forCellReuseIdentifier: "idCellChatRecieved")
        tblView.estimatedRowHeight = 78
        tblView.rowHeight = UITableViewAutomaticDimension
        tblView.tableFooterView = UIView(frame: CGRect.zero)
        tblView.delegate = self
        tblView.dataSource = self
        self.imagePicker.delegate = self
        self.locationManager.delegate = self
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(handleKeyboardNotification),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(handleKeyboardNotification),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil
        )
    }
    
    @objc func handleKeyboardNotification(_ notification: Notification) {
        
        if let userInfo = notification.userInfo {
            
            let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
            
            let isKeyboardShowing = notification.name == NSNotification.Name.UIKeyboardWillShow
            
            bottomConstraint?.constant = isKeyboardShowing ? keyboardFrame!.height : 0
            
            if isKeyboardShowing == false{
                
                if Utills.instance.isIphoneX(){
                    
                    bottomConstraint.constant = 20
                }else{
                    
                    bottomConstraint.constant = 0
                }
            }
            
            UIView.animate(withDuration: 0, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                
                self.view.layoutIfNeeded()
                
            }, completion:
                
                { (isSuccess) in
                    
                    if self.items.count > 0{
                        
                        self.tblView.scrollToRow(at: IndexPath(row: self.items.count - 1 , section: 0), at: .top, animated: false)
                    }
            })
        }
    }
    
    //Downloads messages
    func fetchData() {
        Message.downloadAllMessages(forUserID: self.currentUser!.id!, completion: {[weak weakSelf = self] (message) in
            weakSelf?.items.append(message)
            weakSelf?.items.sort{ $0.timestamp < $1.timestamp }
            DispatchQueue.main.async {
                if let state = weakSelf?.items.isEmpty, state == false {
                    weakSelf?.tblView.reloadData()
                    weakSelf?.tblView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                }
            }
        })
        //        Message.markMessagesRead(forUserID: self.currentUser!.id)
    }
    
    @objc func dismissSelf() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    func composeMessage(type: MessageType, content: Any)  {
        let message = Message.init(type: type, content: content, owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: false, isReceived: false)
        Message.send(message: message, toID: self.currentUser!.id!, completion: {(_) in
            //self.scrollToBottom()
        })
    }
    
    func checkLocationPermission() -> Bool {
        var state = false
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            state = true
        case .authorizedAlways:
            state = true
        default: break
        }
        return state
    }
    
    func sendFCMNotification(token: String, message: String, userName: String) {
        
        if token == ""{
            
            return
        }
        
        let strUrl = NSString(format: "https://fcm.googleapis.com/fcm/send") as String
        
        let manager = AFHTTPRequestOperationManager()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "application/json","text/html") as Set<NSObject>
        
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer.setValue("key=AAAAKtImTFw:APA91bGOIFufn1CEgGEzX-YhkPuIYoIJoqL1GenEZqeJVbN8IY-2IRXTP06gEMZR_7qfnQ2lT3weER0TuYC1X-M-RU6-82VwrscVRD5D4gxRMWBv-wd_IfvuhjGQgGYUeo4anuxe7IOB", forHTTPHeaderField: "Authorization")
        
        let param: [String: Any] = [
            "to": token,
            "notification": [
                "body": message,
                "title": userName,
                "badge": "1",
                "sound": "default"
            ],
            ]
        manager.post(strUrl,
                     parameters: param,
                     success: { (operation,responseObject) in
                        
                        let element : NSDictionary = responseObject as! NSDictionary
                        
        },
                     failure: { (operation,error) in
                        print(error)
        })
    }
    
    func backGroundRequestForGetReceiverToken(){
        Database.database().reference().child(nodeUsers).child(currentUser?.id ?? "0").observeSingleEvent(of: .value, with: { (snapshot) in
            if !snapshot.exists()
            {
            } else {
                if let fcmToken = (snapshot.value as! NSDictionary)[keyDeviceToken] as? String {
                    self.fcmReceiverToken = fcmToken
                }
            }
        })
    }
    
    func getLoginUsername(){
        guard let uid = Auth.auth().currentUser?.uid else{
            return
        }
        Database.database().reference().child(nodeUsers).queryOrderedByKey().queryEqual(toValue: uid).observe(.childAdded, with: { snapshot in
            if !snapshot.exists() {return}
            var name = ""
            
            if let defaults = (snapshot.value as! NSDictionary)[keyUsername] as? String {
                name =  name + defaults
            }
            self.senderUserName = name
            
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customization()
        self.fetchData()
        self.getLoginUsername()
        self.backGroundRequestForGetReceiverToken()
        self.observeChatUser()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        userDefaults.set("", forKey: keyIsChattingwith)
        userDefaults.set("", forKey: keyCurrentChatUserId)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @objc func respondToSwipeGestureDown(gesture: UIGestureRecognizer) {
        self.txtMessage.resignFirstResponder()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func onClickReject(_ sender: Any) {
        guard let chatUser = self.currentUser, let chatUserUid = chatUser.id, let loginUser = User.loginUser.value, let loginUid = loginUser.id else { return }
        Database.database().reference()
            .child("users")
            .child(loginUid)
            .child("conversations")
            .child(chatUserUid)
            .removeValue()
    }
    
    @IBAction func onClickAccept(_ sender: Any) {
        
        guard let chatUser = self.currentUser, let name = chatUser.name else { return }

        self.composeMessage(type: .text, content: "Hi, " + name)
        
        DispatchQueue.main.async {
            self.sendFCMNotification(token: self.fcmReceiverToken, message: "Hi, " + name, userName: self.senderUserName)
        }
    }
    
    @IBAction func onClick_btnImage(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let actionCamera: UIAlertAction = UIAlertAction(title: "Camera", style: .default) { (action: UIAlertAction) in
            
            self.shareMedia(isOpenCamera: true)
            
        }
        
        let actionGallary: UIAlertAction = UIAlertAction(title: "Gallary", style: .default) { (action: UIAlertAction) in
            
            let pickerController = DKImagePickerController()
            
            pickerController.assetType = DKImagePickerControllerAssetType.allPhotos
            pickerController.sourceType = DKImagePickerControllerSourceType.photo
            pickerController.maxSelectableCount = 5

            
            pickerController.didSelectAssets = { (assets: [DKAsset]) in
                
                for asset in assets{
                
                    asset.fetchOriginalImage(completeBlock: { image, info in
                        if let pickedImage = image {
                            self.composeMessage(type: .photo, content: pickedImage)
                        }
                    })
                }
            }
            
            self.present(pickerController, animated: true)
        }
        
        let actionCancel: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (action: UIAlertAction) in
            
        }
        
        alertController.addAction(actionCamera)
        alertController.addAction(actionGallary)
        alertController.addAction(actionCancel)
        
        // self.present(alertController, animated: true, completion: nil)
        
        self.present(alertController, animated: true, completion:{
            alertController.view.superview?.isUserInteractionEnabled = true
            alertController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertClose(gesture:))))
        })
        
        
    }
    
    func shareMedia(isOpenCamera: Bool){
        
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == .authorized || status == .notDetermined) {
            
            if isOpenCamera{
                
                self.imagePicker.sourceType = .camera;
            }else{
                
                self.imagePicker.sourceType = .savedPhotosAlbum;
            }
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func onClick_btnSend(_ sender: AnyObject) {
        if let text = self.txtMessage.text {
            if text.count > 0 {
                self.composeMessage(type: .text, content: text)
                
                DispatchQueue.main.async {
                    self.sendFCMNotification(token: self.fcmReceiverToken, message: text, userName: self.senderUserName)
                }
                //self.sendFCMNotification(token: self.fcmReceiverToken, message: text, userName: senderUserName)
                self.txtMessage.text = ""
            }
        }
        self.showUnreadMessage = false
    }
    
    @IBAction func onClick_btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
        if let currentUserID = Auth.auth().currentUser?.uid {
            Database.database().reference().child("users").child(currentUserID).child("conversations").child(currentUser!.id!).observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    
                    Database.database().reference().child("conversations").child(location).removeAllObservers()
                    
                }
            })
        }
    }
    
    @IBAction func onClick_btnEmoji(_ sender: Any) {
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnviewProfileClick(_ sender: Any) {
        
        let nextpage = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        
        let userObj = UserDataModel()
        
        if let id = self.currentUser?.id{
            
            userObj.userId = id
        }
        nextpage.userModel = userObj
        nextpage.currentUser = self.currentUser
        nextpage.isViewProfile = true
        self.navigationController?.pushViewController(nextpage, animated: true)
    }
    
    
    @objc func tapView(sender:UITapGestureRecognizer) {
        self.txtMessage.resignFirstResponder()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        txtMessage.becomeFirstResponder()
        txtMessage.textColor = UIColor.darkGray
        viewMessage.backgroundColor = UIColor.white
    }
    
    func textViewDidChange(_ textView: UITextView) {
        setUpTextviewHeight()
    }
    
    func setUpTextviewHeight(){
        let minheight:CGFloat = 50
        let maxheight:CGFloat   = 100
        
        let height:CGFloat = txtMessage.contentSize.height + 5
        
        if height >= minheight{
            if height >= maxheight{
                consviewMessageHeight.constant = maxheight
            }else{
                consviewMessageHeight.constant = height
            }
        }else{
            consviewMessageHeight.constant = minheight
        }
        
    }
    
    @IBAction func onClick_btnBack(sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func longPressChatMsg(_ longPressGestureRecognizer: UILongPressGestureRecognizer) {
        self.txtMessage.resignFirstResponder()
        if longPressGestureRecognizer.state == UIGestureRecognizerState.began {
            
            let touchPoint = longPressGestureRecognizer.location(in: self.tblView)
            if let indexPath = self.tblView.indexPathForRow(at: touchPoint) {
                
                guard items.count > indexPath.row else {
                    return
                }
                
                guard items[indexPath.row] != nil else {
                    return
                }
                
                let msgObj = self.items[indexPath.row]
                
                let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                let actionCopy: UIAlertAction = UIAlertAction(title: "Copy", style: .default) { (action: UIAlertAction) in
                    
                    self.copyMsg(self.items[indexPath.row])
                    
                }
                
                let actionDelete: UIAlertAction = UIAlertAction(title: "Delete", style: .default) { (action: UIAlertAction) in
                    
                    self.deleteMsg(self.items[indexPath.row], indexpath: indexPath)
                }
                
                let actionCancel: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (action: UIAlertAction) in
                    
                }
                var itemAdded = false
                if msgObj.type == .text{
                    alertController.addAction(actionCopy)
                    itemAdded = true
                }
                
                if msgObj.owner == .sender{
                    alertController.addAction(actionDelete)
                    itemAdded = true
                }
                
                if itemAdded == false{
                    
                    return
                }
                
                alertController.addAction(actionCancel)
                
                // self.present(alertController, animated: true, completion: nil)
                
                self.present(alertController, animated: true, completion:{
                    alertController.view.superview?.isUserInteractionEnabled = true
                    alertController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertClose(gesture:))))
                })
            }
        }
    }
    
    func copyMsg(_ msgObj: Message){
        if let message = msgObj.content as? String {
            UIPasteboard.general.string = message
        }
    }
    
    func deleteMsg(_ msgObj: Message, indexpath: IndexPath) {
        
        guard let user = currentUser else {
            return
        }
        
        Message.deleteMessage(forUserID: user.id!, forID: self.items[indexpath.row].messageId)
    }
    
    @objc func alertClose(gesture: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func dayDifference(from interval : TimeInterval) -> String
    {
        let calendar = NSCalendar.current
        let date = Date(timeIntervalSince1970: interval)
        if calendar.isDateInYesterday(date) { return "Yesterday" }
        else if calendar.isDateInToday(date) { return "Today" }
        else if calendar.isDateInTomorrow(date) { return "Tomorrow" }
        else {
            let startOfNow = calendar.startOfDay(for: Date())
            let startOfTimeStamp = calendar.startOfDay(for: date)
            let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
            let day = components.day!
            if day < 1 { return "\(abs(day)) days ago" }
            else { return "In \(day) days" }
        }
    }
    
}

extension ChattingVC :  UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.isDragging {
            cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.3, animations: {
                cell.transform = CGAffineTransform.identity
            })
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let objMsg = self.items[indexPath.row]
        var dateHeaderTitle = ""
        let calendar = NSCalendar.current
        let currentDate : NSDate = NSDate(timeIntervalSince1970: TimeInterval(objMsg.timestamp))
        var isDateVisible = false
        if (indexPath.row == 0)
        {
            isDateVisible = true
        } else {
            let premsg =  self.items[indexPath.row - 1]
            let calendar = NSCalendar.current
            
            // Replace the hour (time) of both dates with 00:00
            let preDate : NSDate = NSDate(timeIntervalSince1970: TimeInterval(premsg.timestamp))
            
            let date1 = calendar.startOfDay(for: preDate as Date)
            let date2 = calendar.startOfDay(for: currentDate as Date)
            let components = calendar.dateComponents([.day], from: date1, to: date2)
            if(components.day == 0)
            {
                isDateVisible = false
            }else
            {
                isDateVisible = true
            }
        }
        
        let diffInDays = Calendar.current.dateComponents([.day,.weekday], from: calendar.startOfDay(for: Date()) , to: currentDate as Date)
        if diffInDays.day == 0 {
            dateHeaderTitle =  self.dayDifference(from: TimeInterval(objMsg.timestamp))//"Today"
        } else if diffInDays.day == 1 {
            dateHeaderTitle =  self.dayDifference(from: TimeInterval(objMsg.timestamp))//"Yesterday"
        } else if diffInDays.day! > 1 && diffInDays.day! < 7 {
            switch diffInDays.weekday {
            case 1:
                dateHeaderTitle = "Sunday"
                break;
            case 2:
                dateHeaderTitle = "Monday"
                break;
            case 3:
                dateHeaderTitle = "Tuesday"
                break;
            case 4:
                dateHeaderTitle = "Wednesday"
                break;
            case 5:
                dateHeaderTitle = "Thursday"
                break;
            case 6:
                dateHeaderTitle = "Friday"
                break;
            case 7:
                dateHeaderTitle = "Saturday"
                break;
            default:
                dateHeaderTitle = "Saturday"
                break;
            }
        }else {
            dateHeaderTitle = Utills.instance.convertDateToString(currentDate as Date, format: "MM/dd/yyyy")
        }
        
        switch self.items[indexPath.row].owner {
        case .receiver:
            
            var showUnreadLine = false
            if self.showUnreadMessage{
                if objMsg.unreadMsgStart{
                    
                    showUnreadLine = true
                }
            }
            
            switch self.items[indexPath.row].type {
                
            case .text:
                let cell = tableView.dequeueReusableCell(withIdentifier: "idCellChatRecieved", for: indexPath) as! ChatCellRecieved
                cell.lblChatMessage.text = self.items[indexPath.row].content as? String
                let currentDate : NSDate = NSDate(timeIntervalSince1970: TimeInterval(self.items[indexPath.row].timestamp))
                cell.lblTime.text = Utills.instance.convertDateToString(currentDate as Date, format: "hh:mm a")
                
                if(!isDateVisible) {
                    cell.dateHeight?.constant = 0
                } else {
                    cell.dateHeight?.constant = 21
                    cell.lblDate.text = dateHeaderTitle
                }
                
                if showUnreadLine {
                    cell.unreadHeight.constant = 27
                }else {
                    cell.unreadHeight.constant = 0
                }
                
                
                return cell
            case .photo:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatImageCell", for: indexPath) as! ChatImageCell
                
                cell.contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
                cell.lblTime.transform = CGAffineTransform(scaleX: -1, y: 1)
                cell.lblUnreadMsg.transform = CGAffineTransform(scaleX: -1, y: 1)
                cell.lblDate.transform = CGAffineTransform(scaleX: -1, y: 1)
                cell.imgMessage.transform = CGAffineTransform(scaleX: -1, y: 1)
                
                cell.imgBlueTick.isHidden = true
                let currentDate : NSDate = NSDate(timeIntervalSince1970: TimeInterval(self.items[indexPath.row].timestamp))
                cell.lblTime.text = Utills.instance.convertDateToString(currentDate as Date, format: "hh:mm a")
                
                if let image = self.items[indexPath.row].image {
                    cell.imgMessage.image = image
                } else {
                    if let strURL = self.items[indexPath.row].content as? String,
                        let url = URL(string: strURL) {
                        cell.imgMessage.sd_setImage(with: url, completed: nil)
                    }
                }
                
                if(!isDateVisible) {
                    cell.dateHeight?.constant = 0
                } else {
                    cell.dateHeight?.constant = 21
                    cell.lblDate.text = dateHeaderTitle
                }
                
                if showUnreadLine {
                    cell.unreadHeight.constant = 27
                }else {
                    cell.unreadHeight.constant = 0
                }
                
                return cell
            case .location:
                //                cell.messageBackground.image = UIImage.init(named: "location")
                //                cell.message.isHidden = true
                return UITableViewCell()
            }
        case .sender:
            switch self.items[indexPath.row].type {
            case .text:
                let cell = tableView.dequeueReusableCell(withIdentifier: "idCellChat", for: indexPath) as! ChatCell
                cell.lblChatMessage.text = self.items[indexPath.row].content as? String
                let currentDate : NSDate = NSDate(timeIntervalSince1970: TimeInterval(self.items[indexPath.row].timestamp))
                
                cell.lblTime.text = Utills.instance.convertDateToString(currentDate as Date, format: "hh:mm a")
    
                if self.items[indexPath.row].isRead && Auth.auth().currentUser?.uid ==  self.items[indexPath.row].fromID {
                    cell.imgBlueTick.isHidden = false
                    cell.imgBlueTick.image = UIImage(named: "read_msg")
                } else {
                    
                    if self.items[indexPath.row].isReceived && Auth.auth().currentUser?.uid ==  self.items[indexPath.row].fromID{
                        
                        cell.imgBlueTick.image = UIImage(named: "receive_msg")
                        cell.imgBlueTick.isHidden = false
                    }else{
                        
                        cell.imgBlueTick.image = UIImage(named: "sent_msg")
                        cell.imgBlueTick.isHidden = false
                    }
                }
                
                if(!isDateVisible) {
                    cell.dateHeight?.constant = 0
                } else {
                    cell.dateHeight?.constant = 21
                    cell.lblDate.text = dateHeaderTitle
                }
                
                return cell
            case .photo:
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatImageCell", for: indexPath) as! ChatImageCell
                
                cell.contentView.transform = .identity
                cell.lblTime.transform = .identity
                cell.lblDate.transform = .identity
                cell.lblUnreadMsg.transform = .identity
                cell.imgMessage.transform = .identity
                
                let currentDate : NSDate = NSDate(timeIntervalSince1970: TimeInterval(self.items[indexPath.row].timestamp))
                cell.lblTime.text = Utills.instance.convertDateToString(currentDate as Date, format: "hh:mm a")
                
                if self.items[indexPath.row].isRead && Auth.auth().currentUser?.uid ==  self.items[indexPath.row].fromID {
                    cell.imgBlueTick.isHidden = false
                    cell.imgBlueTick.image = UIImage(named: "read_msg")
                } else {
                    
                    if self.items[indexPath.row].isReceived && Auth.auth().currentUser?.uid ==  self.items[indexPath.row].fromID{
                        
                        cell.imgBlueTick.image = UIImage(named: "receive_msg")
                        cell.imgBlueTick.isHidden = false
                    }else{
                        
                        cell.imgBlueTick.image = UIImage(named: "sent_msg")
                        cell.imgBlueTick.isHidden = false
                    }
                }
                
                if let image = self.items[indexPath.row].image {
                    cell.imgMessage.image = image
                } else {
                    if let strURL = self.items[indexPath.row].content as? String,
                        let url = URL(string: strURL) {
                        cell.imgMessage.sd_setImage(with: url, completed: nil)
                    }
                }
                
                if(!isDateVisible) {
                    cell.dateHeight?.constant = 0
                } else {
                    cell.dateHeight?.constant = 21
                    cell.lblDate.text = dateHeaderTitle
                }
                
                return cell
            case .location:
                return UITableViewCell()
            }
        }
    }
}

extension ChattingVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.composeMessage(type: .photo, content: pickedImage)
        } else {
            let pickedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            self.composeMessage(type: .photo, content: pickedImage)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //    func scrollToBottom(){
    //        DispatchQueue.main.async {
    //            let indexPath = IndexPath(row: self.items.count-1, section: 0)
    //            self.tblView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    //            self.tblView.setContentOffset(.zero, animated: false)
    //        }
    //    }
    
}

//extension UITableView {
//
//    func scrollToBottom(){
//
//        DispatchQueue.main.async {
//            let indexPath = IndexPath(
//                row: self.numberOfRows(inSection:  self.numberOfSections-1) - 1,
//                section: self.numberOfSections - 1)
//            self.scrollToRow(at: indexPath, at: .bottom, animated: true)
//        }
//    }
//
//    func scrollToTop() {
//
//        DispatchQueue.main.async {
//            let indexPath = IndexPath(row: 0, section: 0)
//            self.scrollToRow(at: indexPath, at: .top, animated: false)
//        }
//    }
//}

extension ChattingVC {
    
    func observeChatUser() {
        guard let user = self.currentUser, let uid = user.id else { return }
        
        Database.database().reference()
            .child("users")
            .child(uid)
            .observe(.value) { (snapshot) in
                self.userCurrentStatus.text = ""
                
                if let dict = snapshot.value as? [String:Any]{
                    
                    if let token = dict[keyDeviceToken] as? String{
                        
                        self.fcmReceiverToken = token
                    }
                }
                
                guard let chatUser = Mapper<User>().map(JSONObject: snapshot.value) else {return}
                
                if chatUser.email != self.currentUser?.email{
                    return
                }
                
                if let onlineStatus = chatUser.onlineStatus,
                    let isAllowed = onlineStatus.isAllowed, isAllowed,
                    let isOnline = onlineStatus.isOnline, isOnline {
                    self.userCurrentStatus.text = "online"
                    
                    // if i disable lastseen and online so i won't be able to see others
                    guard let login = User.loginUser.value else { return }
                    if let onlineStatus = login.onlineStatus,
                        let isOnlineAllowed = onlineStatus.isAllowed, isOnlineAllowed{
                        
                    }else {
                        self.userCurrentStatus.text = ""
                    }
                    
                }else if let lastSeen = chatUser.lastSeen,
                    let isAllowed = lastSeen.isAllowed, isAllowed,
                    let timestamp = lastSeen.timestamp {
                    if let timeAgo = NSDate(timeIntervalSince1970: Double(timestamp/1000)).formattedAsTimeAgo() {
                        self.userCurrentStatus.text = "last seen \(timeAgo)"
                    }
                    
                    // if i disable lastseen and online so i won't be able to see others
                    guard let login = User.loginUser.value else { return }
                    if let lastSeen = login.lastSeen,
                        let isLastSeenAllowed = lastSeen.isAllowed, isLastSeenAllowed{
                    }else {
                        self.userCurrentStatus.text = ""
                    }
                }
        }
        
        User.loginUser.asObservable().subscribe { (user) in
            guard let login = User.loginUser.value else { return }

            if let onlineStatus = login.onlineStatus,
                let isOnlineAllowed = onlineStatus.isAllowed, isOnlineAllowed,
                let isOnline = onlineStatus.isOnline, isOnline,
                let lastSeen = login.lastSeen,
                let isLastSeenAllowed = lastSeen.isAllowed, isLastSeenAllowed{
                
            }else {
                //disable for handle appdidbecome active again
               // self.userCurrentStatus.text = ""
            }
            
            if let conversations = login.conversations, let conversation = conversations[self.currentUser?.id ?? "0"] {
                if conversation.location != nil {
                    self.requestView.isHidden = true
                    self.viewMessage.isUserInteractionEnabled = true
                }else if conversation.friendRequestTimestamp != nil {
                    self.requestView.isHidden = false
                    self.viewMessage.isUserInteractionEnabled = false
                    self.requestTitle.text = (self.currentUser?.name ?? "") + " sent request to start the conversation."
                }
            }else {
                self.requestView.isHidden = true
                self.viewMessage.isUserInteractionEnabled = true
            }
        }.disposed(by: self.disposeBag)
    }
    
}
