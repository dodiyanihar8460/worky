
import UIKit

class MessagesTableViewCell: UITableViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLastMessage: UILabel!
    @IBOutlet weak var badge: BadgeSwift!
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib() {
//        badge.textColor = UIColor.white
//        badge.backgroundColor = UIColor.red
//        badge.font = UIFont(name: "HelveticaNeue-Medium", size: CGFloat(13))
//        badge.borderWidth = 0
//        badge.insets = CGSize(width: 0, height: 0)
//        badge.layer.cornerRadius = badge.frame.height / 2
        
        
        badge.textColor = UIColor.white
        badge.font = UIFont(name: "HelveticaNeue-Medium", size: CGFloat(13))
        badge.borderWidth = 0
        badge.insets = CGSize(width: 0, height: 0)
        badge.minimumScaleFactor = 0.2
        
        imgUser.layer.cornerRadius = 45/2
    }

}
