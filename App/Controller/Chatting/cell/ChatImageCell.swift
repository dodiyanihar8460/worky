//
//  ChatImageCell.swift
//  Worky
//
//  Created by Bhavik Thummar on 27/11/18.
//  Copyright © 2018 Bhavesh-Patel. All rights reserved.
//

import UIKit

class ChatImageCell: UITableViewCell {
    
    @IBOutlet weak var imgMessage: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgBlueTick: UIImageView!
    
    @IBOutlet weak var unreadHeight: NSLayoutConstraint!
    @IBOutlet weak var dateHeight: NSLayoutConstraint!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblUnreadMsg: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgMessage.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapImage))
        
        lblDate?.layer.cornerRadius = (lblDate?.frame.height)!/2
        lblDate?.layer.masksToBounds = true
        
        imgMessage.addGestureRecognizer(tapGesture)
        
    }
    
    @objc func tapImage() {
        var images = [SKPhoto]()
        let photo = SKPhoto.photoWithImage(imgMessage.image ?? UIImage())// add some UIImage
        images.append(photo)
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(0)
        AppDelegate.getAppDelegate().window?.rootViewController?.present(browser, animated: true, completion: {})
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imgMessage.image = #imageLiteral(resourceName: "ic_Emoji")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
