//
//  ChatCell.swift
//  SocketChat
//
//  Created by Gabriel Theodoropoulos on 1/31/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//
import UIKit

class ChatCellRecieved: BaseCell {

    @IBOutlet weak var unreadHeight: NSLayoutConstraint!
    @IBOutlet weak var dateHeight: NSLayoutConstraint!
    @IBOutlet weak var lblChatMessage: UILabel!
    @IBOutlet weak var lblTime: UILabel!

    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblDate?.layer.cornerRadius = (lblDate?.frame.height)!/2
        lblDate?.layer.masksToBounds = true
        dateHeight.constant = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
