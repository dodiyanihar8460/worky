//
//  userViewCell.swift
//  Swril
//
//  Created by Bhavesh patel on 8/8/18.
//  Copyright © 2018 Birger LLC. All rights reserved.
//

import UIKit

class userViewCell: UITableViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgUser.layer.cornerRadius = 50/2

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
