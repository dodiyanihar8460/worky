//
//  UserVC.swift
//  App
//
//  Created by Bhavesh patel on 7/10/18.
//  Copyright © 2018 Birger LLC. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import SDWebImage
import FirebaseDatabase

class UserVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var mainArray = NSMutableArray()
    var ref: DatabaseReference!
    var arrSearchHistory = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       searchBar.delegate = self
        
        ref = Database.database().reference()
        let nib = UINib(nibName: "userViewCell", bundle: nil)
        tblView.register(nib, forCellReuseIdentifier: "userViewCell")
        tblView.delegate = self
        tblView.dataSource = self
        tblView.rowHeight = UITableViewAutomaticDimension
        tblView.tableFooterView = UIView(frame: CGRect.zero)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.txtSearch.delegate = self
        // Do any additional setup after loading the view.
        
        Conversation.showConversations { (conversations) in
            //
        }
        
        User.observeLoginUser()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if let arr = userDefaults.value(forKey: kSearchHistory) as? NSArray {
            
            let arrData = arr.mutableCopy() as! NSMutableArray
            var arrUserIds = [String]()
            self.arrSearchHistory.removeAllObjects()
            for value in arrData{
              
                let userObj = UserDataModel().decodeJson((value as? [String : Any])!)
                   
                if arrUserIds.contains(userObj.userId) == false{
                    
                    arrUserIds.append(userObj.userId)
                    self.arrSearchHistory.add(value)
                }
            }
        }
        self.txtSearch.text = ""
        tblView.reloadData()
        //getUsers()
        userDefaults.set("", forKey: keyIsChattingwith)
        userDefaults.set("", forKey: keyCurrentChatUserId)
    }
    
    //MARK: Button Action
    
    @IBAction func onClick_btnSearch(_ sender: Any) {
        if txtSearch.text! != "" {
            self.getUsers(txtSearch.text!)
        }else{
            self.mainArray.removeAllObjects()
            self.tblView.reloadData()
        }
    }
    

    //MARK: - API Call
    func getUsers(_ text : String)
    {
        guard let uid = Auth.auth().currentUser?.uid else{
            return
        }
        
        self.ref.child(nodeUsers).queryOrdered(byChild: keyUsername).queryStarting(atValue: text.capitalized).queryEnding(atValue: text.capitalized+"\u{f8ff}").observe(.value, with: { snapshot in
            
            self.ref.child(nodeUsers).queryOrdered(byChild: keyUniqueName).queryStarting(atValue: text.capitalized).observe(.value, with: { snapshot1 in
                
                if (snapshot.exists() == false && snapshot1.exists() == false)
                {
                    self.mainArray.removeAllObjects()
                    self.tblView.reloadData()
                    return
                }
                var userIDs = NSArray()
                
                if let defaults = (snapshot.value as?  NSDictionary) {
                    userIDs = defaults.allKeys as NSArray
                }
                
                userIDs =  userIDs.filter { ($0 as! String) != uid } as NSArray
                
                self.mainArray.removeAllObjects()
                for i  in userIDs{
                   
                    let model = UserDataModel()
                    model.userId  = "\(i)"
                    
                    let user = (snapshot.value as! NSDictionary).value(forKey: i as! String) as! NSDictionary
                    
                    if let defaults = user.value(forKey: keyEmail) as? String {
                        model.email = defaults
                    }
                    
                    if let defaults = user.value(forKey: keyUsername) as? String {
                        model.userName = defaults
                    }
                    
                    if let defaults = user.value(forKey: keyProfilePic) as? String {
                        model.profilePic = defaults
                    }
                    
                    if let defaults = user.value(forKey: keyisPrivateAccount) as? Bool {
                        model.isPrivateAccount = defaults
                    }
                    
                    self.mainArray.add(model)
                }
                
                
                var userIDs1 = NSArray()
                
                if let defaults = (snapshot1.value as?  NSDictionary) {
                    userIDs1 = defaults.allKeys as NSArray
                }
                
                userIDs1 =  userIDs1.filter { ($0 as! String) != uid } as NSArray
                
                for i  in userIDs1{
                    
                    let model = UserDataModel()
                    model.userId  = "\(i)"
                    
                    let user = (snapshot1.value as! NSDictionary).value(forKey: i as! String) as! NSDictionary
                    
                    if let defaults = user.value(forKey: keyEmail) as? String {
                        model.email = defaults
                    }
                    
                    if let defaults = user.value(forKey: keyUsername) as? String {
                        model.userName = defaults
                    }
                    
                    if let defaults = user.value(forKey: keyProfilePic) as? String {
                        model.profilePic = defaults
                    }
                    
                    if let defaults = user.value(forKey: keyisPrivateAccount) as? Bool {
                        model.isPrivateAccount = defaults
                    }
                    
                    if let defaults = user.value(forKey: keyUniqueName) as? String {
                        model.uniqueName = defaults
                        
                        if defaults.localizedCaseInsensitiveContains(text) == false{
                            continue
                        }
                    }else{
                        continue
                    }
                    
                    if self.mainArray.contains(model) == false{
                        
                        self.mainArray.add(model)
                    }
                }
                self.tblView.reloadData()
                
                })
        })
    }
}

extension UserVC : UITextFieldDelegate
{
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text! != "" {
            self.getUsers(textField.text!)
        } else {
           self.tblView.reloadData()
        }
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text! != "" {
            self.getUsers(textField.text!+string)
        }else{
            self.mainArray.removeAllObjects()
            self.tblView.reloadData()
        }
        return true
    }
}

extension UserVC : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if searchBar.text! != "" {
            self.getUsers(searchText)
        }else{
            self.mainArray.removeAllObjects()
            self.tblView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text! != "" {
            self.getUsers(searchBar.text!)
        }else{
            self.mainArray.removeAllObjects()
            self.tblView.reloadData()
        }
    }
}

extension  UserVC : UITableViewDelegate, UITableViewDataSource
{
    //MARK: UITableView Delegate & Data Source
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (searchBar.text?.isEmpty)!{
            return arrSearchHistory.count
        }else{
            return mainArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (searchBar.text?.isEmpty)!{
            
            if self.arrSearchHistory.count > indexPath.row{
            
                let userObj = UserDataModel().decodeJson(arrSearchHistory.object(at: indexPath.row) as! [String : Any])
                let cell = tableView.dequeueReusableCell(withIdentifier: "userViewCell", for: indexPath) as! userViewCell
                //var userObj : UserDataModel = UserDataModel()
                //userObj = mainArray.object(at: indexPath.row) as! UserDataModel
                cell.lblName.text = "\(userObj.userName)"
                cell.imgUser.image = #imageLiteral(resourceName: "ic_defaultProfile")
                cell.imgUser.sd_setShowActivityIndicatorView(true)
                cell.imgUser.sd_setIndicatorStyle(.gray)
                cell.imgUser.sd_setImage(with: URL(string: userObj.profilePic), completed: { (image, error, cacheType, url) in
                    if (error != nil) {
                        cell.imgUser.image = #imageLiteral(resourceName: "ic_defaultProfile")
                    }
                })
                cell.imgUser.layer.cornerRadius = cell.imgUser.frame.width/2
                cell.imgUser.layer.masksToBounds = true
                cell.selectionStyle = .none
                return cell
            }else{
                
                return UITableViewCell()
            }

        } else {
            
             if self.mainArray.count > indexPath.row{
            
                let cell = tableView.dequeueReusableCell(withIdentifier: "userViewCell", for: indexPath) as! userViewCell
                var userObj : UserDataModel = UserDataModel()
                userObj = mainArray.object(at: indexPath.row) as! UserDataModel
                cell.lblName.text = "\(userObj.userName)"
                cell.imgUser.image = #imageLiteral(resourceName: "ic_defaultProfile")
                cell.imgUser.sd_setShowActivityIndicatorView(true)
                cell.imgUser.sd_setIndicatorStyle(.gray)
                cell.imgUser.sd_setImage(with: URL(string: userObj.profilePic), completed: { (image, error, cacheType, url) in
                    if (error != nil) {
                        cell.imgUser.image = #imageLiteral(resourceName: "ic_defaultProfile")
                    }
                })
                cell.imgUser.layer.cornerRadius = cell.imgUser.frame.width/2
                cell.imgUser.layer.masksToBounds = true
                cell.selectionStyle = .none
                return cell
             }else{
                
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if (searchBar.text?.isEmpty)!{
//            let userObj = UserDataModel().decodeJson(arrSearchHistory.object(at: indexPath.row) as! [String : Any])
//            let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let nextpage = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
//            nextpage.userModel = userObj
//            nextpage.isViewProfile = true
//            self.navigationController?.pushViewController(nextpage,animated: true)
//        } else {
//            var userObj : UserDataModel = UserDataModel()
//            userObj = mainArray.object(at: indexPath.row) as! UserDataModel
//            let searchData = userObj.encodeToJSONForSearch()
//            
//            if self.arrSearchHistory.contains(searchData) {
//                self.arrSearchHistory.remove(searchData)
//            }
//            self.arrSearchHistory.insert(searchData, at: 0)
//            
//            userDefaults.set(self.arrSearchHistory, forKey: kSearchHistory)
//            let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let nextpage = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
//            nextpage.userModel = userObj
//            nextpage.isViewProfile = true
//            self.navigationController?.pushViewController(nextpage,animated: true)
//        }
        
        if (searchBar.text?.isEmpty)!{
            
            if self.arrSearchHistory.count > indexPath.row{
            
                let userObj = UserDataModel().decodeJson(arrSearchHistory.object(at: indexPath.row) as! [String : Any])
                let nextpage = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                nextpage.userModel = userObj

                nextpage.currentUser = User(name: userObj.userName, email: userObj.email, id: userObj.userId, profilePic: userObj.profilePic,  isPrivateAccount: userObj.isPrivateAccount, uniqueName : userObj.uniqueName)
                nextpage.isViewProfile = true
                self.navigationController?.pushViewController(nextpage, animated: true)
                }
        } else {
            
            if self.mainArray.count > indexPath.row{
            
                var userObj : UserDataModel = UserDataModel()
                userObj = mainArray.object(at: indexPath.row) as! UserDataModel
                let searchData = userObj.encodeToJSONForSearch()
                
                if self.arrSearchHistory.contains(searchData) {
                    self.arrSearchHistory.remove(searchData)
                }
                self.arrSearchHistory.insert(searchData, at: 0)
                
                userDefaults.set(self.arrSearchHistory, forKey: kSearchHistory)
                let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                let nextpage = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                nextpage.userModel = userObj
                nextpage.currentUser = User(name: userObj.userName, email: userObj.email, id: userObj.userId, profilePic: userObj.profilePic, isPrivateAccount: userObj.isPrivateAccount, uniqueName : userObj.uniqueName)
                nextpage.isViewProfile = true
                self.navigationController?.pushViewController(nextpage, animated: true)
            }
        }
    }
}
