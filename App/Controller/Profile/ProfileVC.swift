//
//  ProfileVC.swift
//  Swril
//
//  Created by Bhavesh patel on 8/8/18.
//  Copyright © 2018 Birger LLC. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import ObjectMapper

class ProfileVC: UIViewController,NVActivityIndicatorViewable {
    
    @IBOutlet weak var btnSignOut: UIBarButtonItem!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtAbout: UITextView!
    
    @IBOutlet weak var txtEduction: UITextField!
    @IBOutlet weak var txtWork: UITextField!
    @IBOutlet weak var txtSports: UITextField!
    @IBOutlet weak var txtHobbies: UITextField!
    @IBOutlet weak var txtMoreInfo: UITextField!
    
    @IBOutlet weak var lblName: UITextField!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnAddPic: UIButton!
    
    @IBOutlet weak var constraintTxtEductionHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintTxtWorkHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintTxtSportsHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintTxtHobbiesHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintTxtMoreInfoHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintTxtAboutHeight: NSLayoutConstraint!
    
    @IBOutlet weak var constraintTxtEductionBottomSpace: NSLayoutConstraint!
    @IBOutlet weak var constraintTxtWorkBottomSpace: NSLayoutConstraint!
    @IBOutlet weak var constraintTxtSportsBottomSpace: NSLayoutConstraint!
    @IBOutlet weak var constraintTxtHobbiesBottomSpace: NSLayoutConstraint!
    
    var ref: DatabaseReference!
    var profileID = ""
    var isViewProfile = false
    var userModel = UserDataModel()
    var isImageselected = false
    
    var currentUser: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference()
        imgUser.layer.cornerRadius = imgUser.frame.height/2
        imgUser.layer.masksToBounds =  true
        imgUser.layer.borderWidth = 5.0
        imgUser.layer.borderColor = #colorLiteral(red: 0.4606202411, green: 0.4606202411, blue: 0.4606202411, alpha: 0.4)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGestureDown))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
   
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        // Do any additional setup after loading the view.
        
        // listen for searched user changes
        guard let searchUser = self.currentUser, let searchUID = searchUser.id,
            let loginUser = User.loginUser.value, let loginUID = loginUser.id else { return }
        Database.database().reference()
            .child("users")
            .child(searchUID)
            .observe(.value) { (snapshot) in
                guard let chatUser = Mapper<User>().map(JSONObject: snapshot.value) else {return}
         
                // for private account
                if let isPrivateAccount = chatUser.isPrivateAccount, isPrivateAccount {
                    if let conversations = chatUser.conversations,
                        let conversation = conversations[loginUID] {
                        
                        // 1
                        if conversation.location == nil && conversation.friendRequestTimestamp != nil {
                            print("pending")
                            self.btnMessage.setBackgroundImage(nil, for: .normal)
                            self.btnMessage.backgroundColor = colourTheme
                            self.btnMessage.setTitle("PENDING", for: .normal)
                        }else if conversation.location != nil {
                            print("message")
                            self.btnMessage.setBackgroundImage(UIImage(named: "ic_messageUser"), for: .normal)
                            self.btnMessage.setTitle("", for: .normal)
                            self.btnMessage.backgroundColor = nil
                        }else {
                            
                        }
                        
                    }else {
                        print("request")
                        self.btnMessage.setBackgroundImage(nil, for: .normal)
                        self.btnMessage.backgroundColor = colourTheme
                        self.btnMessage.setTitle("REQUEST", for: .normal)
                    }
//                    switch isPrivateAccount {
//                    case true:
//                        //  if account is private than check do you already have conversation?
//                        self.btnMessage.setBackgroundImage(nil, for: .normal)
//                        self.btnMessage.setTitle("Request", for: .normal)
//                    case false:
//                        break
//                    }
                }
        }
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        userDefaults.set("", forKey: keyIsChattingwith)
        userDefaults.set("", forKey: keyCurrentChatUserId)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        guard let uid = Auth.auth().currentUser?.uid else{
            return
        }
        if(!isViewProfile) {
            btnMessage.isHidden = true
            setData(uid : uid)
            
            btnSignOut.isEnabled = true
            btnSignOut.tintColor = UIColor.white
        } else {
            btnSignOut.isEnabled = false
            btnSignOut.tintColor = UIColor.clear
            btnAddPic.isHidden = true
            btnMessage.isHidden = false
            self.btnBack.setTitle("", for: .normal)
            self.btnBack.setImage(#imageLiteral(resourceName: "ic_back"), for: .normal)
            self.lblName.text = self.userModel.userName
            self.txtAbout.text = self.userModel.about
            
            self.txtEduction.text = self.userModel.education
            self.txtWork.text = self.userModel.work
            self.txtSports.text = self.userModel.sports
            self.txtHobbies.text = self.userModel.hobbies
            self.txtMoreInfo.text = self.userModel.moreInfo
            
            self.lblName.isUserInteractionEnabled = false
            self.txtAbout.isUserInteractionEnabled = false
            
            self.txtEduction.isUserInteractionEnabled = false
            self.txtWork.isUserInteractionEnabled = false
            self.txtSports.isUserInteractionEnabled = false
            self.txtHobbies.isUserInteractionEnabled = false
            self.txtMoreInfo.isUserInteractionEnabled = false
            
            setData(uid : self.userModel.userId)
            
            self.imgUser.sd_setShowActivityIndicatorView(true)
            self.imgUser.sd_setIndicatorStyle(.gray)
            self.imgUser.sd_setImage(with: URL(string: self.userModel.profilePic), completed: { (image, error, cacheType, url) in
                if (error != nil) {
                    self.imgUser.image = #imageLiteral(resourceName: "ic_defaultProfile")
                }
            })
            //setData(uid : profileID)
        }
        
        self.setDesign()
    }
    
    func setDesign()
    {
        
        
        txtEduction.attributedPlaceholder = NSAttributedString(string:txtEduction.placeholder!,
                                                               attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtEduction.font!])
        txtWork.attributedPlaceholder = NSAttributedString(string:txtWork.placeholder!,
                                                           attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtWork.font!])
        
        txtSports.attributedPlaceholder = NSAttributedString(string:txtSports.placeholder!,
                                                             attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtSports.font!])
        
        txtHobbies.attributedPlaceholder = NSAttributedString(string:txtHobbies.placeholder!,
                                                              attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtHobbies.font!])
        txtMoreInfo.attributedPlaceholder = NSAttributedString(string:txtMoreInfo.placeholder!,
                                                               attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtMoreInfo.font!])
        
        
        //Add bottom layer and tint color
//        txtEduction.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
//        txtWork.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
//        txtSports.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
//        txtHobbies.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
//        txtMoreInfo.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        
        txtEduction.tintColor = colourTheme
        txtWork.tintColor = colourTheme
        txtSports.tintColor = colourTheme
        txtHobbies.tintColor = colourTheme
        txtMoreInfo.tintColor = colourTheme
        
        //Set delegate
        //        txtEduction.delegate = self
        //        txtWork.delegate = self
        //        txtSports.delegate = self
        //        txtHobbies.delegate = self
        //        txtMoreInfo.delegate = self
        
        Utills.instance.txtleftViewImage(txtEduction, #imageLiteral(resourceName: "ic_education"))
        Utills.instance.txtleftViewImage(txtWork, #imageLiteral(resourceName: "ic_work"))
        Utills.instance.txtleftViewImage(txtSports, #imageLiteral(resourceName: "ic_sports"))
        Utills.instance.txtleftViewImage(txtHobbies, #imageLiteral(resourceName: "ic_hobbies"))
        Utills.instance.txtleftViewImage(txtMoreInfo, #imageLiteral(resourceName: "ic_moreInfo"))
    }
    
    @objc func respondToSwipeGestureDown(gesture: UIGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    deinit {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func setData(uid : String)
    {
        let size = CGSize(width: 30, height:30)
        startAnimating(size, message: nil, type: NVActivityIndicatorType(rawValue: 22)!)
        
        self.stopAnimating()
        ref.child(nodeUsers).queryOrderedByKey().queryEqual(toValue: uid).observe(.childAdded, with: { snapshot in
            if !snapshot.exists() {return}
            
            self.userModel.userId = snapshot.key
            if let defaults = (snapshot.value as! NSDictionary)[keyUsername] as? String {
                self.userModel.userName = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyAbout] as? String {
                self.userModel.about = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyEducation] as? String {
                self.userModel.education = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyWork] as? String {
                self.userModel.work = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keySports] as? String {
                self.userModel.sports = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyHobbies] as? String {
                self.userModel.hobbies = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyMoreInfo] as? String {
                self.userModel.moreInfo = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyEmail] as? String {
                self.userModel.email = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyProfilePic] as? String {
                self.userModel.profilePic = defaults
            }
            
            self.lblName.text = self.userModel.userName
            self.txtAbout.text = self.userModel.about
            
            self.txtEduction.text = self.userModel.education
            self.txtWork.text = self.userModel.work
            self.txtSports.text = self.userModel.sports
            self.txtHobbies.text = self.userModel.hobbies
            self.txtMoreInfo.text = self.userModel.moreInfo
            
            if self.userModel.education == ""{
                self.constraintTxtEductionHeight.constant = 0
                self.constraintTxtEductionBottomSpace.constant = 0
            }else{
                self.constraintTxtEductionHeight.constant = 61
                self.constraintTxtEductionBottomSpace.constant = 5
            }
            
            if self.userModel.work == ""{
                self.constraintTxtWorkHeight.constant = 0
                self.constraintTxtWorkBottomSpace.constant = 0
            }else{
                self.constraintTxtWorkHeight.constant = 61
                self.constraintTxtWorkBottomSpace.constant = 5
            }
            
            if self.userModel.sports == ""{
                self.constraintTxtSportsHeight.constant = 0
                self.constraintTxtSportsBottomSpace.constant = 0
            }else{
                self.constraintTxtSportsHeight.constant = 61
                self.constraintTxtSportsBottomSpace.constant = 5
            }
            
            if self.userModel.hobbies == ""{
                self.constraintTxtHobbiesHeight.constant = 0
                self.constraintTxtHobbiesBottomSpace.constant = 0
            }else{
                self.constraintTxtHobbiesHeight.constant = 61
                self.constraintTxtHobbiesBottomSpace.constant = 5
            }
            
            if self.userModel.moreInfo == ""{
                self.constraintTxtMoreInfoHeight.constant = 0
            }else{
                self.constraintTxtMoreInfoHeight.constant = 61
            }
            
            if self.userModel.about == ""{
                self.constraintTxtAboutHeight.constant = 0
            }else{
                self.constraintTxtAboutHeight.constant = 60
            }
            
            self.imgUser.sd_setShowActivityIndicatorView(true)
            self.imgUser.sd_setIndicatorStyle(.gray)
            self.imgUser.sd_setImage(with: URL(string: self.userModel.profilePic), completed: { (image, error, cacheType, url) in
                if (error != nil) {
                    self.imgUser.image = #imageLiteral(resourceName: "ic_defaultProfile")
                }
            })
            //self.txtDOB.text = self.userModel.profilePic
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func editProfile() {
        let size = CGSize(width: 30, height:30)
        startAnimating(size, message: nil, type: NVActivityIndicatorType(rawValue: 22)!)
        guard let uid = Auth.auth().currentUser?.uid else{
            return
        }
        uploadImages(userId: uid,imagesArray : [self.imgUser.image!]){ (uploadedImageUrlsArray) in
            self.stopAnimating()
            self.userModel.userName = self.lblName.text!.capitalized
            self.userModel.about = self.txtAbout.text!
            
            self.userModel.education = self.txtEduction.text!
            self.userModel.work = self.txtWork.text!
            self.userModel.sports = self.txtSports.text!
            self.userModel.hobbies = self.txtHobbies.text!
            self.userModel.moreInfo = self.txtMoreInfo.text!
            
            self.userModel.profilePic = uploadedImageUrlsArray[0]
            self.ref.child(nodeUsers).child(self.userModel.userId).updateChildValues(self.userModel.encodeToJSON())
            let alert = UIAlertController(title: "", message: "Profile updated successfully.", preferredStyle: .alert)
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 2
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
        }
        
    }
   
    @IBAction func onClick_btnMessage(_ sender: Any) {
        guard let user = currentUser, let uid = user.id else { return }
        
        if self.btnMessage.titleLabel?.text == "PENDING" {
            print("Pending")
            return
        }

        if self.btnMessage.titleLabel?.text == "REQUEST" {
            User.sentFriendshipRequest(userId: uid)
            print("send request")
            return
        }
    
        let vc = storyboard?.instantiateViewController(withIdentifier: "ChattingVC") as! ChattingVC
        
//        user.profilePic = UIImage()
//        var setImge = false
//        if let url = URL(string: userModel.profilePic){
//
//            let data = try? Data(contentsOf: url)
//            if data != nil{
//
//                if let img = UIImage(data: data!){
//
//                    user.profilePic = img
//                    setImge = true
//                }
//            }
//        }
//
//        if setImge == false{
//
//            if let img = UIImage(named: "ic_defaultProfile") {
//
//                user.profilePic = img
//            }
//        }
        
        vc.currentUser = user
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func onClick_btnEdit(_ sender: Any) {
        if !isViewProfile {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func onClick_btnSignPut(_ sender: Any) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as? SettingsVC{
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func onClick_btnAdd(_ sender: Any) {
        let alertImage = UIAlertController(title: "Select Option", message: nil, preferredStyle: .actionSheet)
        //Create an action
        let camera = UIAlertAction(title: "Take new Photo", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if !UIImagePickerController.isSourceTypeAvailable(.camera) {
                let alert = UIAlertController(title: "Error", message: "Camera not available.", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                })
                alert.addAction(defaultAction)
                self.present(alert, animated: true)
            }else{
                self.tirarNovaFoto()
            }
        })
        let imageGallery = UIAlertAction(title: "Select from gallery", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.selecionarFoto()
        })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
        })
        alertImage.addAction(camera)
        alertImage.addAction(imageGallery)
        alertImage.addAction(cancel)
        
        alertImage.popoverPresentationController?.sourceView = (sender as! UIButton)
        alertImage.popoverPresentationController?.sourceRect = (sender as! UIButton).bounds
        
        present(alertImage, animated: true)
    }
    
    func tirarNovaFoto() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .camera
        present(picker, animated: true)
    }
    
    func selecionarFoto() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        present(picker, animated: true)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func uploadImages(userId: String, imagesArray : [UIImage], completionHandler: @escaping ([String]) -> ()){
        let storage =  Storage.storage()
        var uploadedImageUrlsArray = [String]()
        var uploadCount = 0
        let imagesCount = imagesArray.count
        
        for image in imagesArray{
            
            let imageName =  String(arc4random()) + ((NSString(format: "%.0f.jpg", Date().timeIntervalSince1970) as NSString) as String) // Unique string to reference image
            
            //Create storage reference for image
            let storageRef = storage.reference().child("\(userId)").child(imageName)
            
            let myImage = image
            guard let uplodaData = UIImagePNGRepresentation(myImage) else{
                return
            }
            
            // Upload image to firebase
            let uploadTask = storageRef.putData(uplodaData, metadata: nil, completion: { (metadata, error) in
                if error != nil{
                    print(error)
                    return
                }
           storageRef.downloadURL(completion: { (imageUrl, err) in
            uploadedImageUrlsArray.append((imageUrl?.absoluteString)!)
                    uploadCount += 1
                    if uploadCount == imagesCount{
                        completionHandler(uploadedImageUrlsArray)
                    }
                })
            })
            observeUploadTaskFailureCases(uploadTask : uploadTask)
        }
    }
    
    func observeUploadTaskFailureCases(uploadTask :  StorageUploadTask ){
        uploadTask.observe(.failure) { snapshot in
            
            if let error = snapshot.error as NSError? {
                var msg = ""
                switch (StorageErrorCode(rawValue: error.code)!) {
                case .objectNotFound:
                    msg = "File doesn't exist"
                    break
                case .unauthorized:
                    msg = "User doesn't have permission to access file"
                    break
                case .cancelled:
                    msg = "User cancelled the upload"
                    break
                    
                case .unknown:
                    msg = "Unknown error occurred, please try again"
                    break
                default:
                    msg = "Something went wrong while uploading images please try again."
                    break
                }
                self.stopAnimating()
                let alert = UIAlertController(title: "", message:msg, preferredStyle: .alert)
                self.present(alert, animated: true, completion: nil)
                let when = DispatchTime.now() + 3
                DispatchQueue.main.asyncAfter(deadline: when){
                    alert.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

extension ProfileVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIActionSheetDelegate{
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if var pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            pickedImage = self.resizeImage(image: pickedImage, newWidth: 200.0)
            
            self.imgUser.image = pickedImage
            self.isImageselected = true
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}
