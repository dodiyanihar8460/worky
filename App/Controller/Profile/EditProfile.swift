//
//  EditProfile.swift
//  Swril
//
//  Created by Bhavesh patel on 8/16/18.
//  Copyright © 2018 Birger LLC. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class EditProfile: UIViewController,NVActivityIndicatorViewable,UIGestureRecognizerDelegate {
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtAbout: UITextView!
    @IBOutlet weak var txtEduction: UITextField!
    @IBOutlet weak var txtWork: UITextField!
    @IBOutlet weak var txtSports: UITextField!
    @IBOutlet weak var txtHobbies: UITextField!
    @IBOutlet weak var txtMoreInfo: UITextField!
    @IBOutlet weak var lblName: UITextField!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnAddPic: UIButton!
    @IBOutlet var scrView: UIScrollView!
    
    
    var ref: DatabaseReference!
    var profileID = ""
    var isViewProfile = false
    var userModel = UserDataModel()
    var isImageselected = false
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        imgUser.layer.cornerRadius = imgUser.frame.height/2
        imgUser.layer.masksToBounds =  true
        imgUser.layer.borderWidth = 5.0
        imgUser.layer.borderColor = #colorLiteral(red: 0.4606202411, green: 0.4606202411, blue: 0.4606202411, alpha: 0.4)
        
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGestureDown))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
        
        guard let uid = Auth.auth().currentUser?.uid else{
            return
        }
        if(!isViewProfile) {
            btnMessage.isHidden = true
            setData(uid : uid)
        } else {
            btnAddPic.isHidden = true
            btnMessage.isHidden = true
            self.btnBack.setTitle("", for: .normal)
            self.btnBack.setImage(#imageLiteral(resourceName: "ic_back"), for: .normal)
            self.lblName.text = self.userModel.userName
            self.txtAbout.text = self.userModel.about
           
            self.txtEduction.text = self.userModel.education
            self.txtWork.text = self.userModel.work
            self.txtSports.text = self.userModel.sports
            self.txtHobbies.text = self.userModel.hobbies
            self.txtMoreInfo.text = self.userModel.moreInfo
            
            self.lblName.isUserInteractionEnabled = false
            self.txtAbout.isUserInteractionEnabled = false
           
            self.txtEduction.isUserInteractionEnabled = false
            self.txtWork.isUserInteractionEnabled = false
            self.txtSports.isUserInteractionEnabled = false
            self.txtHobbies.isUserInteractionEnabled = false
            self.txtMoreInfo.isUserInteractionEnabled = false
            
            
            self.imgUser.sd_setShowActivityIndicatorView(true)
            self.imgUser.sd_setIndicatorStyle(.gray)
            self.imgUser.sd_setImage(with: URL(string: self.userModel.profilePic), completed: { (image, error, cacheType, url) in
                if (error != nil) {
                    self.imgUser.image = #imageLiteral(resourceName: "ic_defaultProfile")
                }
            })
            //setData(uid : profileID)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        // Do any additional setup after loading the view.
        
//
//        //KeyBoard Observer
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //KeyBoard Observer
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow(_:)),
            name: Notification.Name.UIKeyboardWillShow,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide(_:)),
            name: Notification.Name.UIKeyboardWillHide,
            object: nil
        )

        self.setDesign()
    }
    
    func setDesign()
    {
        
        
        txtEduction.attributedPlaceholder = NSAttributedString(string:txtEduction.placeholder!,
                                                               attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtEduction.font!])
        txtWork.attributedPlaceholder = NSAttributedString(string:txtWork.placeholder!,
                                                               attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtWork.font!])
        
        txtSports.attributedPlaceholder = NSAttributedString(string:txtSports.placeholder!,
                                                            attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtSports.font!])
        
        txtHobbies.attributedPlaceholder = NSAttributedString(string:txtHobbies.placeholder!,
                                                              attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtHobbies.font!])
        txtMoreInfo.attributedPlaceholder = NSAttributedString(string:txtMoreInfo.placeholder!,
                                                                      attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtMoreInfo.font!])
    
        
        //Add bottom layer and tint color
        txtEduction.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtWork.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtSports.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtHobbies.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtMoreInfo.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        
        txtEduction.tintColor = colourTheme
        txtWork.tintColor = colourTheme
        txtSports.tintColor = colourTheme
        txtHobbies.tintColor = colourTheme
        txtMoreInfo.tintColor = colourTheme
        
        //Set delegate
//        txtEduction.delegate = self
//        txtWork.delegate = self
//        txtSports.delegate = self
//        txtHobbies.delegate = self
//        txtMoreInfo.delegate = self
        
        Utills.instance.txtleftViewImage(txtEduction, #imageLiteral(resourceName: "ic_education"))
        Utills.instance.txtleftViewImage(txtWork, #imageLiteral(resourceName: "ic_work"))
        Utills.instance.txtleftViewImage(txtSports, #imageLiteral(resourceName: "ic_sports"))
        Utills.instance.txtleftViewImage(txtHobbies, #imageLiteral(resourceName: "ic_hobbies"))
        Utills.instance.txtleftViewImage(txtMoreInfo, #imageLiteral(resourceName: "ic_moreInfo"))
        
        //Dismiss keyboard
        let tapTerm : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapView(_:)))
        tapTerm.delegate = self
        tapTerm.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapTerm)
        self.view.setNeedsLayout()
    }
    
    //MARK: - UITextField Delegate Method
    @objc func tapView(_ sender:UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    //MARK: - KeyBoard Observer Method
    func adjustInsetForKeyboardShow(_ show: Bool, notification: Notification) {
        let userInfo = notification.userInfo ?? [:]
        let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let adjustmentHeight = (keyboardFrame.height + 20) * (show ? 1 : 0)
        scrView.contentInset.bottom = adjustmentHeight
       // scrView.scrollIndicatorInsets.bottom = adjustmentHeight
    }

    @objc func keyboardWillShow(_ notification: Notification) {
        adjustInsetForKeyboardShow(true, notification: notification)
    }

    @objc func keyboardWillHide(_ notification: Notification) {
        adjustInsetForKeyboardShow(false, notification: notification)
    }

    
//    //MARK: - KeyBoard Observer Method
//    @objc func keyboardWillShow(_ notification:Notification){
//        scrView.isScrollEnabled = true
//        var userInfo = (notification as NSNotification).userInfo!
//        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
//        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
//        var contentInset:UIEdgeInsets = scrView.contentInset
//        contentInset.bottom = keyboardFrame.size.height
//        scrView.contentInset = contentInset
//    }
//
//    @objc func keyboardWillHide(_ notification:Notification){
//        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
//        scrView.contentInset = contentInset
//        scrView.isScrollEnabled = false
//    }

    @objc func respondToSwipeGestureDown(gesture: UIGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    deinit {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func setData(uid : String)
    {
        let size = CGSize(width: 30, height:30)
        startAnimating(size, message: nil, type: NVActivityIndicatorType(rawValue: 22)!)
        
        self.stopAnimating()
        ref.child(nodeUsers).queryOrderedByKey().queryEqual(toValue: uid).observe(.childAdded, with: { snapshot in
            if !snapshot.exists() {return}
            
            self.userModel.userId = snapshot.key
            if let defaults = (snapshot.value as! NSDictionary)[keyUsername] as? String {
                self.userModel.userName = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyAbout] as? String {
                self.userModel.about = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyEducation] as? String {
                self.userModel.education = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyWork] as? String {
                self.userModel.work = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keySports] as? String {
                self.userModel.sports = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyHobbies] as? String {
                self.userModel.hobbies = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyMoreInfo] as? String {
                self.userModel.moreInfo = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyEmail] as? String {
                self.userModel.email = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyProfilePic] as? String {
                self.userModel.profilePic = defaults
            }
            
            self.lblName.text = self.userModel.userName
            self.txtAbout.text = self.userModel.about
            
            self.txtEduction.text = self.userModel.education
            self.txtWork.text = self.userModel.work
            self.txtSports.text = self.userModel.sports
            self.txtHobbies.text = self.userModel.hobbies
            self.txtMoreInfo.text = self.userModel.moreInfo
            
            self.imgUser.sd_setShowActivityIndicatorView(true)
            self.imgUser.sd_setIndicatorStyle(.gray)
            self.imgUser.sd_setImage(with: URL(string: self.userModel.profilePic), completed: { (image, error, cacheType, url) in
                if (error != nil) {
                    self.imgUser.image = #imageLiteral(resourceName: "ic_defaultProfile")
                }
            })
            //self.txtDOB.text = self.userModel.profilePic
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func editProfile() {
        let size = CGSize(width: 30, height:30)
        startAnimating(size, message: nil, type: NVActivityIndicatorType(rawValue: 22)!)
        guard let uid = Auth.auth().currentUser?.uid else{
            return
        }
        
        let storage = Storage.storage()
        let url = self.userModel.profilePic
        if url != "", let storageRef = storage.reference(forURL: url) as? StorageReference {
            //Removes image from storage
            storageRef.delete { error in
                if let error = error {
                    print(error)
                } else {
                    // File deleted successfully
                }
            }
        }
        
        uploadImages(userId: uid,imagesArray : [self.imgUser.image!]){ (uploadedImageUrlsArray) in
            self.stopAnimating()
            self.userModel.userName = self.lblName.text!.capitalized
            self.userModel.about = self.txtAbout.text!
            
            self.userModel.education = self.txtEduction.text!
            self.userModel.work = self.txtWork.text!
            self.userModel.sports = self.txtSports.text!
            self.userModel.hobbies = self.txtHobbies.text!
            self.userModel.moreInfo = self.txtMoreInfo.text!
            
            self.userModel.profilePic = uploadedImageUrlsArray[0]
            self.ref.child(nodeUsers).child(self.userModel.userId).updateChildValues(self.userModel.encodeToJSON())
            let alert = UIAlertController(title: "", message: "Profile updated successfully.", preferredStyle: .alert)
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 2
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
    @IBAction func onClick_btnMessage(_ sender: Any) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextpage = storyboard.instantiateViewController(withIdentifier: "ChattingVC") as! ChattingVC
        // nextpage.userModel = userObj
        // nextpage.isViewProfile = true
//        nextpage.UserName = self.userModel.userName
//        nextpage.ReceiverUserid = self.userModel.userId
//        nextpage.isfromConversatin = true
        //nextpage.CurrentchatID = self.userModel.chatID
        self.navigationController?.pushViewController(nextpage,animated: true)
    }
    
    
    @IBAction func onClick_btnEdit(_ sender: Any) {
        if !isViewProfile {
            if (self.lblName.text?.count)! > 0 {
                self.editProfile()
            } else {
                Utills.instance.showAlertWithTitle("Please enter your name.", strTitle: AlertTitle)
            }
        } else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func onClick_btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func onClick_btnRemoveProfilePic(_ sender: Any) {
        
        self.imgUser.image = UIImage(named: "ic_defaultProfile")
    }
    
    @IBAction func onClick_btnAdd(_ sender: Any) {
        let alertImage = UIAlertController(title: "Select Option", message: nil, preferredStyle: .actionSheet)
        //Create an action
        let camera = UIAlertAction(title: "Take new Photo", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if !UIImagePickerController.isSourceTypeAvailable(.camera) {
                let alert = UIAlertController(title: "Error", message: "Camera not available.", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                })
                alert.addAction(defaultAction)
                self.present(alert, animated: true)
            }else{
                self.tirarNovaFoto()
            }
        })
        let imageGallery = UIAlertAction(title: "Select from gallery", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.selecionarFoto()
        })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
        })
        alertImage.addAction(camera)
        alertImage.addAction(imageGallery)
        alertImage.addAction(cancel)
        
        alertImage.popoverPresentationController?.sourceView = (sender as! UIButton)
        alertImage.popoverPresentationController?.sourceRect = (sender as! UIButton).bounds
        
        present(alertImage, animated: true)
    }
    
    func tirarNovaFoto() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .camera
        present(picker, animated: true)
    }
    
    func selecionarFoto() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        present(picker, animated: true)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func uploadImages(userId: String, imagesArray : [UIImage], completionHandler: @escaping ([String]) -> ()){
        let storage =  Storage.storage()
        var uploadedImageUrlsArray = [String]()
        var uploadCount = 0
        let imagesCount = imagesArray.count
        for image in imagesArray{
            let imageName = String(arc4random()) + ((NSString(format: "%.0f.jpg", Date().timeIntervalSince1970) as NSString) as String) // Unique string to reference image
            
            
            
            storage.reference().child("\(userId)").delete { (err) in
                if err != nil {
                    print("Deleted")
                } else {
                    print(err?.localizedDescription)
                }
            }
            
            
            //Create storage reference for image
            let storageRef = storage.reference().child("\(userId)").child(imageName)

            let myImage = image
            guard let uplodaData = UIImagePNGRepresentation(myImage) else{
                return
            }

            // Upload image to firebase
            let uploadTask = storageRef.putData(uplodaData, metadata: nil, completion: { (metadata, error) in
                if error != nil{
                    print(error)
                    return
                }
                storageRef.downloadURL(completion: { (imageUrl, err) in
                    uploadedImageUrlsArray.append((imageUrl?.absoluteString)!)
                    uploadCount += 1
                    if uploadCount == imagesCount{
                        completionHandler(uploadedImageUrlsArray)
                    }
                })
            })
            observeUploadTaskFailureCases(uploadTask : uploadTask)
        }
    }
    
    func observeUploadTaskFailureCases(uploadTask :  StorageUploadTask ){
        uploadTask.observe(.failure) { snapshot in
            
            if let error = snapshot.error as NSError? {
                var msg = ""
                switch (StorageErrorCode(rawValue: error.code)!) {
                case .objectNotFound:
                    msg = "File doesn't exist"
                    break
                case .unauthorized:
                    msg = "User doesn't have permission to access file"
                    break
                case .cancelled:
                    msg = "User cancelled the upload"
                    break
                    
                case .unknown:
                    msg = "Unknown error occurred, please try again"
                    break
                default:
                    msg = "Something went wrong while uploading images please try again."
                    break
                }
                self.stopAnimating()
                let alert = UIAlertController(title: "", message:msg, preferredStyle: .alert)
                self.present(alert, animated: true, completion: nil)
                let when = DispatchTime.now() + 3
                DispatchQueue.main.asyncAfter(deadline: when){
                    alert.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

extension EditProfile : UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIActionSheetDelegate{
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if var pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            pickedImage = self.resizeImage(image: pickedImage, newWidth: 200.0)
            
            self.imgUser.image = pickedImage
            self.isImageselected = true
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}
