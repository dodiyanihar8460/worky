//
//  ChangePersonalDetailVC.swift
//  Worky
//
//  Created by Nikunj on 18/12/18.
//  Copyright © 2018 Bhavesh-Patel. All rights reserved.
//


import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class ChangePersonalDetailVC: UIViewController,UITextFieldDelegate,UIGestureRecognizerDelegate,NVActivityIndicatorViewable {
    //UIcontrol Outlets
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtUniqueName: UITextField!
    
    @IBOutlet weak var txtPasswordConfirm: UITextField!
    
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var scrView: UIScrollView!
    
    var ref: DatabaseReference!
    var username = ""
    var password = ""
    var userId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        // Do any additional setup after loading the view, typically from a nib.
        setDesign()
        
        guard let uid = Auth.auth().currentUser?.uid else{
            return
        }
        self.setData(uid : uid)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGestureDown))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setDesign()
    }
    
    @objc func respondToSwipeGestureDown(gesture: UIGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func setData(uid : String)
    {
        let size = CGSize(width: 30, height:30)
        startAnimating(size, message: nil, type: NVActivityIndicatorType(rawValue: 22)!)
        
        self.stopAnimating()
        ref.child(nodeUsers).queryOrderedByKey().queryEqual(toValue: uid).observe(.childAdded, with: { snapshot in
            if !snapshot.exists() {return}
            
            self.userId = snapshot.key
            
            if let defaults = (snapshot.value as! NSDictionary)[keyUsername] as? String {
                self.txtUserName.text = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyUniqueName] as? String {
                self.txtUniqueName.text = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyEmail] as? String {
               self.txtEmail.text = defaults
                self.username = defaults
            }
            
            if let defaults = (snapshot.value as! NSDictionary)[keyPassword] as? String {
                self.txtPassword.text = defaults
                self.txtPasswordConfirm.text = defaults
                self.password = defaults
            }
        })
    }
    
    //MARK: -  Set designs for different devices
    func setDesign()
    {
        
        
        txtUserName.attributedPlaceholder = NSAttributedString(string:txtUserName.placeholder!,
                                                               attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtUserName.font!])
        txtPassword.attributedPlaceholder = NSAttributedString(string:txtPassword.placeholder!,
                                                               attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtPassword.font!])
        
        txtEmail.attributedPlaceholder = NSAttributedString(string:txtEmail.placeholder!,
                                                            attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtEmail.font!])
        
        txtUniqueName.attributedPlaceholder = NSAttributedString(string:txtUniqueName.placeholder!,
                                                              attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtUniqueName.font!])
        txtPasswordConfirm.attributedPlaceholder = NSAttributedString(string:txtPasswordConfirm.placeholder!,
                                                                      attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtPasswordConfirm.font!])
        
        
        //Add Corner radius
        btnSignUp.layer.cornerRadius =  btnSignUp.frame.height/2
        btnSignUp.layer.masksToBounds =  true
        
        //Add bottom layer and tint color
        txtUserName.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtPassword.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtEmail.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtUniqueName.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtPasswordConfirm.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        
        txtUserName.tintColor = colourTheme
        txtPassword.tintColor = colourTheme
        txtEmail.tintColor = colourTheme
        txtUniqueName.tintColor = colourTheme
        txtPasswordConfirm.tintColor = colourTheme
        
        //Set delegate
        txtUserName.delegate = self
        txtPassword.delegate = self
        txtEmail.delegate = self
        txtUniqueName.delegate = self
        txtPasswordConfirm.delegate = self
        
        Utills.instance.txtleftViewImage(txtPassword, #imageLiteral(resourceName: "ic_Password"))
        Utills.instance.txtleftViewImage(txtUserName, #imageLiteral(resourceName: "ic_username"))
        Utills.instance.txtleftViewImage(txtUniqueName, #imageLiteral(resourceName: "ic_username"))
        Utills.instance.txtleftViewImage(txtEmail, #imageLiteral(resourceName: "ic_email"))
        Utills.instance.txtleftViewImage(txtPasswordConfirm, UIImage())

        //Dismiss keyboard
        let tapTerm : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapView(_:)))
        tapTerm.delegate = self
        tapTerm.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapTerm)
        self.view.setNeedsLayout()
    }
    
    //MARK: - UITextField Delegate Method
    @objc func tapView(_ sender:UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    //MARK: - UITextField Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtEmail{
            
            if textField.text != ""  {
                let newLength = textField.text!.count + string.count - range.length
                return newLength <= 40
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.addBorder(edge: .bottom, color:  UIColor.darkGray, thickness: 1.0)
        // textField.becomeFirstResponder()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let nextTag = textField.tag + 1;
        
        let nextResponder = self.scrView.viewWithTag(nextTag) as? JVFloatLabeledTextField
        if (nextResponder != nil)   {
            nextResponder?.becomeFirstResponder()
        }else{
            self.view.endEditing(true)
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtUserName{
            Utills.instance.emptyFieldValidation(txtUserName)
        }
        else if textField == txtPassword{
            Utills.instance.emptyFieldValidation(txtPassword)
        }
        else if textField == txtEmail{
            Utills.instance.emptyFieldValidation(txtEmail)
        } else if textField == txtUniqueName{
            Utills.instance.emptyFieldValidation(txtUniqueName)
        }
        else if textField == txtPasswordConfirm{
            Utills.instance.emptyFieldValidation(txtPasswordConfirm)
        }
    }
    
    func ValidateTextField() -> Bool
    {
        if(txtUserName.text == "" || txtPassword.text == "" || txtEmail.text == "" || txtUniqueName.text == "" || txtPasswordConfirm.text == "" )
        {
            Utills.instance.emptyFieldValidation(txtUserName)
            Utills.instance.emptyFieldValidation(txtPassword)
            Utills.instance.emptyFieldValidation(txtEmail)
            Utills.instance.emptyFieldValidation(txtUniqueName)
            Utills.instance.emptyFieldValidation(txtPasswordConfirm)
            return false
        }
        return true
    }
    
    @IBAction func onClick_btnSignUp(_ sender: Any) {
        if(ValidateTextField())
        {
            txtUserName.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
            txtPassword.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
            txtEmail.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
            
            txtUniqueName.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
            txtPasswordConfirm.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
            
            if txtPassword.text != txtPasswordConfirm.text {
                txtPasswordConfirm.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
                txtPassword.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
                Utills.instance.showAlertWithTitle("Password Does not match", strTitle: AlertTitle)
                return
            }
            
            if(txtEmail.text?.isValidEmail())!
            {
                if (txtPassword.text!.utf16).count >= 6{
                    
                    var parameter = [String:Any]()
                    parameter[keyUsername] = self.txtUserName.text!
                    parameter[keyUniqueName] = self.txtUniqueName.text!
                    parameter[keyPassword] = self.txtPassword.text!
                    
                    var responseReceived = false
                    let size = CGSize(width: 30, height:30)
                    self.startAnimating(size, message: nil, type: NVActivityIndicatorType(rawValue: 22)!)
                    
                    self.ref.child(nodeUsers).queryOrdered(byChild: keyUniqueName).queryStarting(atValue: txtUniqueName.text?.capitalized).observe(.value, with: { snapshot in
                        
                        if responseReceived == false{
                            responseReceived = true
                            var userIDs = NSArray()
                            
                            if let defaults = (snapshot.value as?  NSDictionary) {
                                userIDs = defaults.allKeys as NSArray
                            }
                            
                            
                            var nameFound = false
                            
                            if let uid = Auth.auth().currentUser?.uid{
                            
                                userIDs =  userIDs.filter { ($0 as! String) != uid } as NSArray
                                
                                for i  in userIDs{
                                    
                                    let user = (snapshot.value as! NSDictionary).value(forKey: i as! String) as! NSDictionary
                                    
                                    if let defaults = user.value(forKey: keyUniqueName) as? String {
                                        
                                        if defaults.lowercased() == self.txtUniqueName.text?.lowercased(){
                                            
                                            self.stopAnimating()
                                            Utills.instance.showAlertWithTitle("Username already exists.", strTitle: AlertTitle)
                                            nameFound = true
                                            break;
                                        }
                                    }
                                }
                            }
                            
                            if nameFound == false{
                                
                                if self.password == self.txtPassword.text!{
                                    
                                    self.ref.child(nodeUsers).child(self.userId).updateChildValues(parameter)
                                    
                                    self.stopAnimating()
                                    let alert = UIAlertController(title: "", message: "Personal Detail Updated successfully.", preferredStyle: .alert)
                                    self.present(alert, animated: true, completion: nil)
                                    let when = DispatchTime.now() + 2
                                    DispatchQueue.main.asyncAfter(deadline: when){
                                        alert.dismiss(animated: true, completion: nil)
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                    return
                                }
                                
                                if let currentUser = Auth.auth().currentUser{
                                    
                                    let credential = EmailAuthProvider.credential(withEmail: self.username, password: self.password)
                                    currentUser.reauthenticateAndRetrieveData(with: credential) { authResult, error in
                                        
                                        if error == nil {
                                            
                                            currentUser.updatePassword(to: self.txtPassword.text!, completion: { (error) in
                                                
                                                if error == nil {
                                                    self.ref.child(nodeUsers).child(self.userId).updateChildValues(parameter)
                                                    
                                                    self.stopAnimating()
                                                    let alert = UIAlertController(title: "", message: "Personal Detail Updated successfully.", preferredStyle: .alert)
                                                    self.present(alert, animated: true, completion: nil)
                                                    let when = DispatchTime.now() + 2
                                                    DispatchQueue.main.asyncAfter(deadline: when){
                                                        alert.dismiss(animated: true, completion: nil)
                                                        self.logout()
                                                    }
                                                }else{
                                                    Utills.instance.showAlertWithTitle((error?.localizedDescription)!, strTitle: AlertTitle)
                                                }
                                            })
                                        }else
                                        {
                                            self.stopAnimating()
                                            Utills.instance.showAlertWithTitle((error?.localizedDescription)!, strTitle: AlertTitle)
                                        }
                                    }
                                }
                            }
                        }
                    })
                    
                }else
                {
                    self.stopAnimating()
                    txtPassword.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
                    Utills.instance.showAlertWithTitle("Password must be at least 6 characters long.", strTitle: AlertTitle)
                }
            }else{
                self.stopAnimating()
                txtEmail.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
                Utills.instance.showAlertWithTitle("Please enter valid email.", strTitle: AlertTitle)
            }
        }else
        {
            Utills.instance.showAlertWithTitle("All fields are required.", strTitle: AlertTitle)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func logout(){
        
        try! Auth.auth().signOut()
        
        var deviceToken = ""
        if let Token = UserDefaults.standard.value(forKey: keyDeviceToken) as? String
        {
            deviceToken = Token
        }
        
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        
        UserDefaults.standard.set(deviceToken, forKey: keyDeviceToken)
        UserDefaults.standard.synchronize()
        if let storyboard = self.storyboard {
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.present(vc, animated: false, completion: nil)
        }
    }
}
