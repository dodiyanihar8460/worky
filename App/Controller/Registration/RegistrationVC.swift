
import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class RegistrationVC: UIViewController,UITextFieldDelegate,UIGestureRecognizerDelegate,NVActivityIndicatorViewable {
    //UIcontrol Outlets
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtUniqueName: UITextField!
    @IBOutlet weak var txtSurname: UITextField!
    
    @IBOutlet weak var txtEmailConfirm: UITextField!
   
    @IBOutlet weak var txtPasswordConfirm: UITextField!

    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var scrView: UIScrollView!
  
    var ref: DatabaseReference!

    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        // Do any additional setup after loading the view, typically from a nib.
        setDesign()
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGestureDown))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
    }

    override func viewDidAppear(_ animated: Bool) {
        setDesign()
    }
    
    @objc func respondToSwipeGestureDown(gesture: UIGestureRecognizer) {
        self.view.endEditing(true)
    }

    //MARK: -  Set designs for different devices
    func setDesign()
    {
       
        
        txtUserName.attributedPlaceholder = NSAttributedString(string:txtUserName.placeholder!,
                                                               attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtUserName.font!])
        txtPassword.attributedPlaceholder = NSAttributedString(string:txtPassword.placeholder!,
                                                               attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtPassword.font!])
        
        txtEmail.attributedPlaceholder = NSAttributedString(string:txtEmail.placeholder!,
                                                               attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtEmail.font!])
        
        txtSurname.attributedPlaceholder = NSAttributedString(string:txtSurname.placeholder!,
                                                               attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtSurname.font!])
        txtUniqueName.attributedPlaceholder = NSAttributedString(string:txtUniqueName.placeholder!,
                                                              attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtUniqueName.font!])
        
        txtPasswordConfirm.attributedPlaceholder = NSAttributedString(string:txtPasswordConfirm.placeholder!,
                                                               attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtPasswordConfirm.font!])
        
        txtEmailConfirm.attributedPlaceholder = NSAttributedString(string:txtEmailConfirm.placeholder!,
                                                            attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtEmailConfirm.font!])
        
                                                                
        

        //set design for iPad and iphone
//        consTextFieldWidths.constant = self.view.frame.width*0.85
//        consTextFieldTrailing.constant = self.view.frame.width*0.15
//        consTextFieldLeading.constant = self.view.frame.width*0.15

        //Add Corner radius
        btnSignUp.layer.cornerRadius =  btnSignUp.frame.height/2
        btnSignUp.layer.masksToBounds =  true

        //Add bottom layer and tint color
        txtUserName.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtPassword.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtEmail.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtSurname.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtUniqueName.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtPasswordConfirm.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtEmailConfirm.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        
        txtUserName.tintColor = colourTheme
        txtPassword.tintColor = colourTheme
        txtEmail.tintColor = colourTheme
        txtSurname.tintColor = colourTheme
        txtUniqueName.tintColor = colourTheme
        txtEmailConfirm.tintColor = colourTheme
        txtPasswordConfirm.tintColor = colourTheme
        
        //Set delegate
        txtUserName.delegate = self
        txtPassword.delegate = self
        txtEmail.delegate = self
        txtSurname.delegate = self
        txtUniqueName.delegate = self
        txtPasswordConfirm.delegate = self
        txtEmailConfirm.delegate = self
        
        Utills.instance.txtleftViewImage(txtPassword, #imageLiteral(resourceName: "ic_Password"))
        Utills.instance.txtleftViewImage(txtUserName, #imageLiteral(resourceName: "ic_username"))
        Utills.instance.txtleftViewImage(txtEmail, #imageLiteral(resourceName: "ic_email"))
        Utills.instance.txtleftViewImage(txtUniqueName, #imageLiteral(resourceName: "ic_username"))
        Utills.instance.txtleftViewImage(txtSurname, UIImage())
        Utills.instance.txtleftViewImage(txtEmailConfirm, UIImage())
        Utills.instance.txtleftViewImage(txtPasswordConfirm, UIImage())
        
//        //KeyBoard Observer
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(self.keyboardWillShow(_:)),
//            name: Notification.Name.UIKeyboardWillShow,
//            object: nil
//        )
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(self.keyboardWillHide(_:)),
//            name: Notification.Name.UIKeyboardWillHide,
//            object: nil
//        )

        //Dismiss keyboard
        let tapTerm : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapView(_:)))
        tapTerm.delegate = self
        tapTerm.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapTerm)
        self.view.setNeedsLayout()
    }

    //MARK: - UITextField Delegate Method
    @objc func tapView(_ sender:UITapGestureRecognizer) {
        self.view.endEditing(true)
    }

//    //MARK: - KeyBoard Observer Method
//    func adjustInsetForKeyboardShow(_ show: Bool, notification: Notification) {
//        let userInfo = notification.userInfo ?? [:]
//        let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
//        let adjustmentHeight = (keyboardFrame.height + 20) * (show ? 1 : 0)
//        scrView.contentInset.bottom = adjustmentHeight
//        scrView.scrollIndicatorInsets.bottom = adjustmentHeight
//    }
//    
//    @objc func keyboardWillShow(_ notification: Notification) {
//        adjustInsetForKeyboardShow(true, notification: notification)
//    }
//    
//    @objc func keyboardWillHide(_ notification: Notification) {
//        adjustInsetForKeyboardShow(false, notification: notification)
//    }
    
    //MARK: - UITextField Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtEmail  || textField == txtEmailConfirm{
            
            if textField.text != ""  {
                let newLength = textField.text!.count + string.count - range.length
                return newLength <= 40
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.addBorder(edge: .bottom, color:  UIColor.darkGray, thickness: 1.0)
       // textField.becomeFirstResponder()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      
        let nextTag = textField.tag + 1;

        let nextResponder = self.scrView.viewWithTag(nextTag) as? JVFloatLabeledTextField
        if (nextResponder != nil)   {
            nextResponder?.becomeFirstResponder()
        }else{
            self.view.endEditing(true)
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtUserName{
            Utills.instance.emptyFieldValidation(txtUserName)
        }
        else if textField == txtPassword{
            Utills.instance.emptyFieldValidation(txtPassword)
        }
        else if textField == txtEmail{
            Utills.instance.emptyFieldValidation(txtEmail)
        } else if textField == txtSurname{
            Utills.instance.emptyFieldValidation(txtSurname)
        } else if textField == txtUniqueName{
            Utills.instance.emptyFieldValidation(txtUniqueName)
        }
        else if textField == txtPasswordConfirm{
            Utills.instance.emptyFieldValidation(txtPasswordConfirm)
        }
        else if textField == txtEmailConfirm{
            Utills.instance.emptyFieldValidation(txtEmailConfirm)
        }
    }

    func ValidateTextField() -> Bool
    {
        if(txtUserName.text == "" || txtPassword.text == "" || txtEmail.text == "" || txtSurname.text == "" || txtUniqueName.text == "" || txtPasswordConfirm.text == "" || txtEmailConfirm.text == "" )
        {
            Utills.instance.emptyFieldValidation(txtUserName)
            Utills.instance.emptyFieldValidation(txtPassword)
            Utills.instance.emptyFieldValidation(txtEmail)
            Utills.instance.emptyFieldValidation(txtSurname)
            Utills.instance.emptyFieldValidation(txtUniqueName)
            Utills.instance.emptyFieldValidation(txtPasswordConfirm)
            Utills.instance.emptyFieldValidation(txtEmailConfirm)
            return false
        }
        return true
    }

    @IBAction func onClick_btnSignUp(_ sender: Any) {
        if(ValidateTextField())
        {
            txtUserName.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
            txtPassword.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
            txtEmail.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
            
            txtSurname.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
            txtUniqueName.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
            txtPasswordConfirm.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
            txtEmailConfirm.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
            
            if txtEmail.text != txtEmailConfirm.text {
                txtEmail.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
                txtEmailConfirm.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
                Utills.instance.showAlertWithTitle("Email Does not match", strTitle: AlertTitle)
                return
            }
            
            if txtPassword.text != txtPasswordConfirm.text {
                txtPasswordConfirm.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
                txtPassword.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
                Utills.instance.showAlertWithTitle("Password Does not match", strTitle: AlertTitle)
                return
            }
            
            if(txtEmail.text?.isValidEmail())!
            {
                if (txtPassword.text!.utf16).count >= 6{

                    let parameter = NSMutableDictionary()
                    parameter.setValue(self.txtUserName.text!.capitalized + " " + self.txtSurname.text!.capitalized, forKey: keyUsername)
                    parameter.setValue(self.txtUniqueName.text!.capitalized, forKey: keyUniqueName)
                    parameter.setValue(self.txtPassword.text, forKey: keyPassword)
                    parameter.setValue(self.txtEmail.text!, forKey: keyEmail)
                    parameter.setValue([keyIsOnline: false,keyIsAllow:true,keyTimeStamp:nil], forKey: keyOnlineStatus)
                    parameter.setValue([keyIsAllow:true,keyTimeStamp:nil], forKey: keyLastSeen)
                    parameter.setValue(false, forKey: keyisPrivateAccount)
                    
                    if let deviceToken = UserDefaults.standard.value(forKey: keyDeviceToken) as? String
                    {
                        parameter.setValue(deviceToken, forKey: keyDeviceToken)
                    }
                    else{
                        parameter.setValue("", forKey: keyDeviceToken)
                    }
                    
                    let size = CGSize(width: 30, height:30)
                    startAnimating(size, message: nil, type: NVActivityIndicatorType(rawValue: 22)!)
                    
                    var responseReceived = false
                    
                    self.ref.child(nodeUsers).queryOrdered(byChild: keyUniqueName).queryStarting(atValue: txtUniqueName.text?.capitalized).observe(.value, with: { snapshot in
                        
                        if responseReceived == false{
                            responseReceived = true
                            var userIDs = NSArray()
                            
                            if let defaults = (snapshot.value as?  NSDictionary) {
                                userIDs = defaults.allKeys as NSArray
                            }
                            
                            var nameFound = false
                            
                            for i  in userIDs{
                                
                                let user = (snapshot.value as! NSDictionary).value(forKey: i as! String) as! NSDictionary
                                
                                if let defaults = user.value(forKey: keyUniqueName) as? String {
                                    
                                    if defaults.lowercased() == self.txtUniqueName.text?.lowercased(){
                                        
                                        self.stopAnimating()
                                        Utills.instance.showAlertWithTitle("Username already exists.", strTitle: AlertTitle)
                                        nameFound = true
                                        break;
                                    }
                                }
                            }
                            
                            if nameFound == false{
                                
                                Auth.auth().createUser(withEmail: self.txtEmail.text!, password: self.txtPassword.text!, completion: { (user, error) in
                                    self.stopAnimating()
                                    if error == nil {
                                        let userInstance = self.ref.child(nodeUsers).child((user?.user.uid)!)
                                        userInstance.setValue(parameter)
                                        _ = self.dismiss(animated: true, completion: {
                                            Utills.instance.showAlertWithTitle("Registered successfully.", strTitle: AlertTitle)
                                        })
                                    }else
                                    {
                                        Utills.instance.showAlertWithTitle((error?.localizedDescription)!, strTitle: AlertTitle)
                                    }
                                })
                            }
                        }
                    })
                }
                else
                {
                    txtPassword.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
                    Utills.instance.showAlertWithTitle("Password must be at least 6 characters long.", strTitle: AlertTitle)
                }
            }else{
                txtEmail.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
                Utills.instance.showAlertWithTitle("Please enter valid email.", strTitle: AlertTitle)
            }
        }else
        {
            Utills.instance.showAlertWithTitle("All fields are required.", strTitle: AlertTitle)
        }
    }



    @IBAction func onClick_btnBack(_ sender: Any) {
        self.dismiss(animated: true,completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
