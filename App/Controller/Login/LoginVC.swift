//
//  ViewController.swift
//  App
//
//  Created by Bhavesh Patel Lab on 20/07/17.
//  Copyright © 2018 Birger LLC Lab. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseDatabase

class LoginVC: AppLandingBaseViewController,UIGestureRecognizerDelegate,UITextFieldDelegate,NVActivityIndicatorViewable {

    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var autoHidingView: AppAutohidingView!
    //Constrains Outlets
    @IBOutlet weak var consLogoFromTop: NSLayoutConstraint!
    @IBOutlet weak var consLoginViewWidth: NSLayoutConstraint!
    
    
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        ref = Database.database().reference()
        setDesign()

        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGestureDown))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    @objc func respondToSwipeGestureDown(gesture: UIGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setDesign()
    }
    
    override func viewDidLayoutSubviews() {
        autoHidingView.hideIfNeeded()
    }
    
    //MARK: -  Set designs for different devices
    func setDesign()
    {
        txtUserName.attributedPlaceholder = NSAttributedString(string:"Email",
                                                               attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5)
                                                                ,NSAttributedStringKey.font:txtUserName.font!])
        
        txtPassword.attributedPlaceholder = NSAttributedString(string:"Password",
                                                               attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtPassword.font!])
        //set design for iPad and iphone
        // consLoginViewWidth.constant = self.view.frame.width*0.85
        
        //        if(UIDevice.current.userInterfaceIdiom == .pad)
        //        {
        //            consLogoFromTop.constant = 250
        //            consLoginViewWidth.constant = self.view.frame.width*0.6
        //        }
        
        //Add Corner radius
        btnSignIn.layer.cornerRadius = btnSignIn.frame.height/2
        btnSignIn.layer.masksToBounds =  true
        
        //Add bottom layer and tint color
        txtPassword.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtUserName.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtPassword.tintColor = colourTheme
        txtUserName.tintColor = colourTheme
        
        //Set delegate
        txtPassword.delegate = self
        txtUserName.delegate = self

        Utills.instance.txtleftViewImage(txtPassword, #imageLiteral(resourceName: "ic_Password"))
        Utills.instance.txtleftViewImage(txtUserName, #imageLiteral(resourceName: "ic_username"))
                // btnSignIn.isEnabled = false
        ///  btnSignIn.alpha = 0.5
        
    }
    
    //MARK: - UITextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let nextTag = textField.tag + 1;
        let nextResponder = self.view.viewWithTag(nextTag) as? UITextField
        if (nextResponder != nil)   {
            nextResponder?.becomeFirstResponder()
        }else{
            self.view.endEditing(true)
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtPassword{
            Utills.instance.emptyFieldValidation(txtPassword)
        }
        else if textField == txtUserName{
            Utills.instance.emptyFieldValidation(txtUserName)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtUserName {
            let invalidCharacters : CharacterSet!
            
            if textField.text != "" {
                let newLength = textField.text!.count + string.count - range.length
                return newLength <= 40
            }
        }
        return true
    }
    
    func textFieldChanged(_ notification: Notification) {
        let email: String = txtUserName.text!
        let password: String? = txtPassword.text
        if(email.isValidEmail())
        {
            txtUserName.layer.addBorder(edge: .bottom, color: colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        }
        if((password?.count ?? 0) > 0)
        {
            txtPassword.layer.addBorder(edge: .bottom, color: colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        }
        if email.isValidEmail() && (password?.count ?? 0) > 0 {
            // btnSignIn.isEnabled = true
            //   btnSignIn.alpha = 1
        }
        else {
            // btnSignIn.isEnabled = false
            //  btnSignIn.alpha = 0.5
        }
    }
    
    func ValidateTextField() -> Bool
    {
        if(txtUserName.text == "" || txtPassword.text == "" )
        {
            Utills.instance.emptyFieldValidation(txtUserName)
            Utills.instance.emptyFieldValidation(txtPassword)
            return false
        }
        return true
    }
    
    @IBAction func onClick_btnSignIn(_ sender: Any) {
        self.view.endEditing(true)
        if(ValidateTextField())
        {
            if(txtUserName.text?.isValidEmail())!
            {
                txtUserName.layer.addBorder(edge: .bottom, color: colourTheme.withAlphaComponent(0.5), thickness: 1.0)
                txtPassword.layer.addBorder(edge: .bottom, color: colourTheme.withAlphaComponent(0.5), thickness: 1.0)
                
                let size = CGSize(width: 30, height:30)
                startAnimating(size, message: nil, type: NVActivityIndicatorType(rawValue: 22)!)
                Auth.auth().signIn(withEmail: txtUserName.text!, password: txtPassword.text!, completion: { (user, error) in
                    self.stopAnimating()
                    if error == nil {
                        self.redirectToHome()
                        if let deviceToken = UserDefaults.standard.value(forKey: keyDeviceToken) as? String
                        {
                         
                            currentloggedInUserId = user?.user.uid ?? ""
                            userDefaults.setValue(true, forKey: keyAppFirstTimeOpen);
                            userDefaults.setValue(self.txtUserName.text!, forKey: keyUserId);
                            userDefaults.setValue(self.txtPassword.text!, forKey: keyPassword);
                            self.ref.child(nodeUsers).child((user?.user.uid)!).updateChildValues([keyDeviceToken:deviceToken,keyPassword:self.txtPassword.text!])
                        }
                        
                    }else
                    {
                        Utills.instance.showAlertWithTitle((error?.localizedDescription)!, strTitle: AlertTitle)
                    }
                })
                
            }else{
                txtUserName.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
                Utills.instance.showAlertWithTitle("Please enter valid email.", strTitle: AlertTitle)
            }
        }else
        {
            Utills.instance.showAlertWithTitle("All fields are required.", strTitle: AlertTitle)
        }
    }
    
    @IBAction func onClick_btnSignUp(_ sender: Any) {
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func redirectToHome()
    {
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        
        let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "HometabbarVC")
        appDelegate.window?.rootViewController = initialViewController
        appDelegate.window?.makeKeyAndVisible()
    }
    
  
//    func setUserData(param: NSMutableDictionary, uid : String)
//    {
//        self.ref?.child(nodeUsers).child(uid).child(keyTerms).observeSingleEvent(of: .value, with: { (snapshot) in
//            print(snapshot.value)
//            if let flag =  snapshot.value as? Bool
//            {
//                if(flag == true){
//                    param.setValue(true, forKey: keyTerms)
//                    let userInstance = self.ref.child(nodeUsers).child(uid)
//                    userInstance.setValue(param)
//                    self.redirectToHome()
//                }else
//                {
//                    param.setValue(false, forKey: keyTerms)
//                    let userInstance = self.ref.child(nodeUsers).child(uid)
//                    userInstance.setValue(param)
//                    self.redirectToTerms()
//                }
//            }else
//            {
//                param.setValue(false, forKey: keyTerms)
//                let userInstance = self.ref.child(nodeUsers).child(uid)
//                userInstance.setValue(param)
//                self.redirectToTerms()
//            }
//        })
//    }
}
