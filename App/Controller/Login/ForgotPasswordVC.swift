//
//  ForgotPasswordVC.swift
//  Swril
//
//  Created by Bhavesh patel on 8/7/18.
//  Copyright © 2018 Birger LLC. All rights reserved.
//

import UIKit
import FirebaseAuth

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtEmail.attributedPlaceholder = NSAttributedString(string:"Your Register Email",
                                                               attributes:[NSAttributedStringKey.foregroundColor: colourTheme.withAlphaComponent(0.5),NSAttributedStringKey.font:txtEmail.font!])
        
        //txtEmail.layer.addBorder(edge: .top, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        txtEmail.layer.addBorder(edge: .bottom, color:  colourTheme.withAlphaComponent(0.5), thickness: 1.0)
        
        Utills.instance.txtleftViewImage(txtEmail,#imageLiteral(resourceName: "ic_email"))
        
        //Add Corner radius
        btnSubmit.layer.cornerRadius = btnSubmit .frame.height/2
        btnSubmit.layer.masksToBounds =  true
        
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGestureDown))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func respondToSwipeGestureDown(gesture: UIGestureRecognizer) {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK: - Button action
    
    @IBAction func onClick_btnSubmit(_ sender: Any) {
        let email: String = txtEmail.text!
        if(email.isValidEmail())
        {
            txtEmail.layer.addBorder(edge: .bottom, color: colourTheme.withAlphaComponent(0.5), thickness: 1.0)
            Auth.auth().sendPasswordReset(withEmail: email) { error in
                Utills.instance.showAlertWithTitle("Please check your Email to reset password link.", strTitle: AlertTitle)
            }
        }
       
    }
    
    @IBAction func onClick_btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
