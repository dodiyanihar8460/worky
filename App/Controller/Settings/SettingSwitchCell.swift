//
//  SettingSwitchCell.swift
//  Worky
//
//  Created by Nasrullah Khan  on 08/12/2018.
//  Copyright © 2018 Bhavesh-Patel. All rights reserved.
//

import UIKit

class SettingSwitchCell: UITableViewCell {

    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var toggle: CustomSwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
