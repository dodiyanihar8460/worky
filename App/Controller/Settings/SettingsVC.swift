//
//  SettingsVC.swift
//  Worky
//
//  Created by Nasrullah Khan  on 08/12/2018.
//  Copyright © 2018 Bhavesh-Patel. All rights reserved.
//

import UIKit
import RxSwift
import Firebase
import FirebaseAuth
import FirebaseDatabase
import UserNotifications
enum SettingItemType {
    case uiswitch, uibutton
}
struct SettingItem {
    var title: String
    var image: UIImage
    var type: SettingItemType
}

class SettingsVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnSignOut: UIButton!
    var items = [
        SettingItem(title: "Personal Details", image: UIImage(named: "ic_username")!, type: .uibutton),
        SettingItem(title: "Private Account", image: UIImage(named: "ic_private_account")!, type: .uiswitch),
        SettingItem(title: "Notifications", image: UIImage(named: "ic_notification")!, type: .uiswitch),
        SettingItem(title: "Last Seen", image: UIImage(named: "ic_lastseen")!, type: .uiswitch),
        SettingItem(title: "Online Status", image: UIImage(named: "ic_online_status")!, type: .uiswitch)]
    
    let disposeBag = DisposeBag()
    var isNotificationEnable = true
    var isReloadingTable = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        btnSignOut.layer.cornerRadius =  btnSignOut.frame.height/2
        btnSignOut.layer.masksToBounds =  true
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        User.loginUser.asObservable().subscribe { (user) in
            self.isReloadingTable = true
            self.tableView.reloadData()
            self.isReloadingTable = false
            }.disposed(by: self.disposeBag)
        
        
        
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            
            if settings.authorizationStatus == .denied{
                
                self.isNotificationEnable = false
                self.isReloadingTable = true
                self.tableView.reloadData()
                self.isReloadingTable = false
            }else{
                
                self.isNotificationEnable = true
                self.isReloadingTable = true
                self.tableView.reloadData()
                self.isReloadingTable = false
            }
        }
    }
    @objc func toggled(_ sender: CustomSwitch) {
        switch sender.tag {
        case 1:
            print("Private Account", sender.isOn)
            User.privateAccountToggle(isAllowed: sender.isOn)
            break;
            
        case 2:
            print("Notifications",sender.isOn)
            if self.isReloadingTable == false{
                self.notificationToggleClick()
            }
            break;
            
        case 3:
            print("Last Seen",sender.isOn)
            User.lastSeenToggle(isAllowed: sender.isOn)
            break;
            
        case 4:
            print("Online Status",sender.isOn)
            User.onlineStatusToggle(isAllowed: sender.isOn)
        default:
            break
        }
    }
    
    func notificationToggleClick(){

        var title = "Disable Notifications"
        
        if self.isNotificationEnable == false{
            title = "Enable Notifications"
        }
        
        let alert = UIAlertController(title: title, message: "Push notifications are required to alert subscribers to new messages", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Go to Settings", style: .default) { _ in
            if let url = URL(string: UIApplicationOpenSettingsURLString){
                
                UIApplication.shared.open(url)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
    }
    
    @IBAction func btnSignOutClick(_ sender: Any) {
        
        if let uid = Auth.auth().currentUser?.uid{
        
             Database.database().reference().child(nodeUsers).child(uid).updateChildValues([keyDeviceToken:""])
        }
        
        User.userSessionManagerForDisconnect()
        Database.database().reference().child(nodeUsers).removeAllObservers()
        
        try! Auth.auth().signOut()
        
        var deviceToken = ""
        if let Token = UserDefaults.standard.value(forKey: keyDeviceToken) as? String
        {
            deviceToken = Token
        }
        
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
       
        UserDefaults.standard.set(deviceToken, forKey: keyDeviceToken)
        
        UserDefaults.standard.synchronize()
        if let storyboard = self.storyboard {
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    @objc func btnChangePersonalDetailClick(_ sender: UIButton, Event event:AnyObject) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePersonalDetailVC") as? ChangePersonalDetailVC{
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension SettingsVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = self.items[indexPath.row]
        switch obj.type {
        case .uibutton:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingButtonCellID") as! SettingButtonCell
            cell.itemImageView.image = obj.image
            cell.name.text = obj.title
            
            cell.changeButton.addTarget(self, action: #selector(btnChangePersonalDetailClick(_: Event:)), for: .touchUpInside)
            
            return cell
        case .uiswitch:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingSwitchCellID") as! SettingSwitchCell
            cell.itemImageView.image = obj.image
            cell.name.text = obj.title
            
            if let loginUser = User.loginUser.value {
                switch obj.title {
                case "Last Seen":
                    if let lastSeen = loginUser.lastSeen, let isAllowed = lastSeen.isAllowed, isAllowed {
                        cell.toggle.isOn = true
                    }else {
                        cell.toggle.isOn = false
                    }
                    break;
                case "Online Status":
                    if let onlineStatus = loginUser.onlineStatus, let isAllowed = onlineStatus.isAllowed, isAllowed {
                        cell.toggle.isOn = true
                    }else {
                        cell.toggle.isOn = false
                    }
                    break;
                    
                case "Private Account":
                    if let isPrivate = loginUser.isPrivateAccount {
                        if isPrivate{
                            cell.toggle.isOn = true
                        }else{
                            cell.toggle.isOn = false
                        }
                    }
                    break;
                    
                case "Notifications":
                
                    if self.isNotificationEnable{
                        if cell.toggle.isOn == false{
                            self.isReloadingTable = true
                            cell.toggle.setOn(on: true, animated: false)
                            self.isReloadingTable = false
                        }
                    }else{
                        if cell.toggle.isOn == true{
                            self.isReloadingTable = true
                            cell.toggle.setOn(on: false, animated: false)
                            self.isReloadingTable = false
                        }
                    }
                    break;
                    
                default:
                    break
                }
            }
            
            cell.toggle.tag = indexPath.row
            cell.toggle.addTarget(self, action: #selector(self.toggled(_:)), for: UIControlEvents.valueChanged)
            
            return cell
        }
    }
}
