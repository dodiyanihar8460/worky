 //
//  AppLandingBaseViewController
//  App
//
//  Created by Bhavesh Patel Lab on 20/07/17.
//  Copyright © 2018 Birger LLC Lab. All rights reserved.
//

#import "AppLandingBaseViewController.h"

static CGFloat kRequiredBottomMargin = 15.0;

@interface AppLandingBaseViewController ()

@property (nonatomic, assign) CGFloat defaultBottomConstraintConstant;

@end

@implementation AppLandingBaseViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
  [self.view addGestureRecognizer:tap];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  self.defaultBottomConstraintConstant = self.lastRequiredVisibleViewBottomConstraint.constant;

  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardFrameChanged:) name:UIKeyboardWillShowNotification object:nil];
    
  //   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldChanged:) name:UITextFieldTextDidChangeNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)showErrorWithTitle:(NSString *)title andMessage:(NSString *)message {
  [self showErrorWithTitle:title andMessage:message andActionHandler:nil];
}

- (void)showErrorWithTitle:(NSString *)title andMessage:(NSString *)message andActionHandler:(void (^)(UIAlertAction *))handler {
 // [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *closeAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Close", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    if (handler) {
      handler(action);
    } 
  }];
  
  [alertController addAction:closeAction];
  [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark - Tap gesture handling
- (void)hideKeyboard {
  [self.view endEditing:YES];
}

#pragma mark - Keyboard notification handling
- (void)keyboardFrameChanged:(NSNotification *)note {
    
    NSTimeInterval interval = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [note.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGRect finalKeyboardFrame = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    finalKeyboardFrame = [self.view convertRect:finalKeyboardFrame fromView:nil];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:curve];
    [UIView setAnimationDuration:interval];
    if (finalKeyboardFrame.origin.y - kRequiredBottomMargin  < CGRectGetMaxY(self.lastRequredVisibleView.frame)) {
        self.lastRequiredVisibleViewBottomConstraint.constant = finalKeyboardFrame.size.height + kRequiredBottomMargin - 1;
        //    self.welcomeMessageLabel.alpha = 0.0;
    } else {
        self.lastRequiredVisibleViewBottomConstraint.constant = self.defaultBottomConstraintConstant;
        //    self.welcomeMessageLabel.alpha = 1.0;
    }
    [self.view layoutIfNeeded];
    [UIView commitAnimations];
}


//#pragma mark - Keyboard notification handling
//- (void)keyboardShow:(NSNotification *)note {
//
//  NSTimeInterval interval = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//  UIViewAnimationCurve curve = [note.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
//
//  CGRect finalKeyboardFrame = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
//  finalKeyboardFrame = [self.view convertRect:finalKeyboardFrame fromView:nil];
//
//  [UIView beginAnimations:nil context:nil];
//  [UIView setAnimationCurve:curve];
//  [UIView setAnimationDuration:interval];
//
//    self.lastRequiredVisibleViewBottomConstraint.constant = self.view.frame.size.height - _defaultBottomConstraintConstant - kRequiredBottomMargin - finalKeyboardFrame.origin.y;
////  [_lastRequredInvisibleView setAlpha:0.0];
//  [self.view layoutIfNeeded];
//  [UIView commitAnimations];
//}
//
//- (void)keyboardHide:(NSNotification *)note {
//
//    NSTimeInterval interval = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//    UIViewAnimationCurve curve = [note.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
//
//    CGRect finalKeyboardFrame = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
//    finalKeyboardFrame = [self.view convertRect:finalKeyboardFrame fromView:nil];
//
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationCurve:curve];
//    [UIView setAnimationDuration:interval];
//    self.lastRequiredVisibleViewBottomConstraint.constant = _defaultBottomConstraintConstant;//
////  [_lastRequredInvisibleView setAlpha:1.0];
//    [self.view layoutIfNeeded];
//    [UIView commitAnimations];
//}

#pragma mark - textfield notification
- (void)textFieldChanged:(NSNotification *)notification {
  //NSLog(@"Overwrite in subclass");
}

@end
