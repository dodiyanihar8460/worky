//
//  AppLandingBaseViewController
//  App
//
//  Created by Bhavesh Patel Lab on 20/07/17.
//  Copyright © 2018 Birger LLC Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

/** 
 ViewController subclass that will handle keyboard hide-show.
 You need to set |activeTextView| to make it scroll to correct position
 **/
@interface AppLandingBaseViewController : UIViewController

@property (nonatomic, strong) UITextField *activeField;

@property (nonatomic, strong) UIView *inView;
/**
 Last view, that must remain  invisible when keyboard is up
 **/
//@property (nonatomic, strong) IBOutlet UIView *lastRequredInvisibleView;

/**
 Last view, that must remain visible when keyboard is up
 **/
@property (nonatomic, strong) IBOutlet UIView *lastRequredVisibleView;

/**
 Bottom constraint of view, that must remain visible when keyboard is up
 **/
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *lastRequiredVisibleViewBottomConstraint;

 /**
 Show error alert with |title| and |message|
 It will also hide any progress huds
 If user presses OK on the action, handler is called
 **/
- (void)showErrorWithTitle:(NSString *)title andMessage:(NSString *)message andActionHandler:(void (^)(UIAlertAction *action))handler;

/**
 Show error alert with |title| and |message|
 It will also hide any progress huds
 If user presses OK on the action, a 'popviewcontroller' is called.
 **/
- (void)showErrorWithTitle:(NSString *)title andMessage:(NSString *)message;
@end
