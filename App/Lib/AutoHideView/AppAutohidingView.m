//
//  AppAutohidingView
//  App
//
//  Created by Bhavesh Patel Lab on 20/07/17.
//  Copyright © 2018 Birger LLC Lab. All rights reserved.
//

#import "AppAutohidingView.h"

@interface AppAutohidingView ()

@property (nonatomic, assign) BOOL animating;

@end

@implementation AppAutohidingView

- (void)hideIfNeeded {
  // Check if it is overlaping other view
  BOOL intersectsWithAnotherView = [self intersectsWithAnotherSubview];
  if (intersectsWithAnotherView) {
    [self animateAlhpaTo:0];
    return;
  }

  
  // TODO only if needed.
//  // Check if it is out of the screen
//  CGPoint topLeft = self.frame.origin;
//  CGPoint topRight = CGPointMake(CGRectGetMaxX(self.frame), self.frame.origin.y);
//  CGPoint botLeft = CGPointMake(self.frame.origin.x, CGRectGetMaxY(self.frame));
//  CGPoint botRight = CGPointMake(CGRectGetMaxX(self.frame), CGRectGetMaxY(self.frame));
  
  [self animateAlhpaTo:1];
}

-(BOOL)intersectsWithAnotherSubview {
  NSArray *subViewsInSuperView = [self.superview subviews];

  for(UIView *subview in subViewsInSuperView) {
    if (subview != self && ![subview isKindOfClass:[UIImageView class]])
      if(CGRectIntersectsRect(self.frame, subview.frame))
        return YES;
  }
  return NO;
}

- (void)animateAlhpaTo:(CGFloat)newAlpha {
  if (self.animating) {
//    return;
  }
  
  self.animating = YES;
  [UIView animateWithDuration:0.1 animations:^{
    self.alpha = newAlpha;
  } completion:^(BOOL finished) {
    self.animating = NO;
  }];
}

@end
