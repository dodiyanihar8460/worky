//
//  MSAutohidingView.h
//  App
//
//  Created by Bhavesh Patel Lab on 20/07/17.
//  Copyright © 2018 Birger LLC Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

/** 
 UIView that will hide automatically if it is overlaping other (non-imageview) view or is partially out of the screen
 **/
@interface AppAutohidingView : UIView

/** 
 call this if superview layout changes
 **/
- (void)hideIfNeeded;

@end
