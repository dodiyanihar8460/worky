//
//  Utility-Extention.swift
//  Worky
//
//  Created by Nikunj on 20/12/18.
//  Copyright © 2018 Bhavesh-Patel. All rights reserved.
//

import Foundation
import Foundation
import AVFoundation
import AVKit


class Utility_Extention: NSObject{
    //MARK: - Validation functions
    static let instance = Utility_Extention()

    func convertDateToString(_ dateToConvert : Date, format : String) -> String {
        let dateFormatter = DateFormatter()
        
        var paramFormat = format
        
        if isTimeFormatIs12Hour() == false{
            
            paramFormat = format.replacingOccurrences(of: "h", with: "H")
            paramFormat = format.replacingOccurrences(of: "a", with: "")
        }
        
        dateFormatter.dateFormat = paramFormat
        dateFormatter.timeZone = TimeZone.current
        //dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let stringDate = dateFormatter.string(from: dateToConvert)
        return stringDate
    }
    
    func convertStringToDate(_ strToConvert : String, dateFormat : String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = TimeZone.current
        // dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let convertedDate = dateFormatter.date(from: strToConvert)
        return convertedDate!
    }
    
    func isTimeFormatIs12Hour() -> Bool {
        let formatStringForHours = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: NSLocale.current)
        let containsA: NSRange? = (formatStringForHours as NSString?)?.range(of: "a")
        let hasAMPM: Bool = Int(containsA?.location ?? 0) != NSNotFound
        return hasAMPM
    }
    
    
}
