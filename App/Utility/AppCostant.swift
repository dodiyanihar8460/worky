//
//  AppCostant.swift
//  App
//
//  Created by Bhavesh Patel lab on 20/07/17.
//  Copyright © 2018 Birger LLC lab. All rights reserved.
//

import Foundation
import UIKit

//MARK: - App colors

//let colourTheme = #colorLiteral(red: 0, green: 0.4431372549, blue: 0.737254902, alpha: 1) //0071BC

let colourTheme = UIColor(hexString: "0071BC")

//let appblueColor = UIColor(colorLiteralRed: 51/255, green: 139/255, blue: 207/255, alpha: 1.0)

////Utilities
//let utils : Utills = Utills()

var currentloggedInUserId = ""

//KeyWords
let AlertTitle = "ChatApp"

//userdefaults key
let kSearchHistory = "searchHistory"

let userDefaults = UserDefaults.standard

//APP DELEGATE
//let appDelegate = UIApplication.shared.delegate! as! AppDelegate

//Firebase Nodes
let nodeUsers = "users"
let nodeListings = "listings"
let nodeListingsRegistered = "listingRegister"
let nodeChats = "chats"
let nodeUserChats = "userChats"
let nodeChatMessages = "chatMessages"


// Error messages
let serverError = "Something went wrong. Please try again"

//Firebase keys
let keyUserId = "userId"
let keyUsername = "username"
let keyUniqueName = "uniqueName" //will user for uniquely identify user
let keyEmail = "email"
let keyPassword = "password"
let keyProfilePic = "profilePic"
let keyAbout = "about"
let keyisPrivateAccount = "isPrivateAccount"
let keyOnlineStatus = "onlineStatus"
let keyLastSeen = "lastSeen"

let keyIsOnline = "isOnline"
let keyTimestamp = "timestamp"
let keyIsAllow = "isAllowed"

let keyEducation = "eduction"
let keyWork = "work"
let keySports = "sports"
let keyHobbies = "hobbies"
let keyMoreInfo = "moreInfo"

let keyDeviceToken = "deviceToken"
let keyBadgeCount = 0

//userdefault Keys

let keyIsChattingwith = "keyIsChattingwith"
let keyCurrentChatUserId = "currentChatUserId"
let keyUserID = "currentUserId"
let keyUserPass = "currentUserPassword"
let keyAppFirstTimeOpen = "appFirstTimeOpend"

//Chatting
let keyChatID = "chatID"
let keyMembers = "members"
let keyLastMessage = "lastMessageSent"
let keyTimeStamp = "timeStamp"
let keyMessage = "message"
let keyViewFlag = "viewFlag"
let keySentBy = "sentBy"
let keyMessageID = "messageID"

