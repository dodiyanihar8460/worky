//
//  Utills.swift
//  App
//
//  Created by Bhavesh Patel on 20/07/17.
//  Copyright © 2018 Birger LLC. All rights reserved.
//
import Foundation
import AVFoundation
import AVKit

class Utills: NSObject{
    //MARK: - Validation functions
    static let instance = Utills()
    
    func emptyFieldValidation(_ textField: UITextField!)
    {
        if(textField.text == "")
        {
            textField.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
        }else
        {
            textField.layer.addBorder(edge: .bottom, color: colourTheme, thickness: 1.0)
        }
    }
    
    func isIphoneX() -> Bool {
        
        if #available(iOS 11.0, *) {
            
            if let wn = AppDelegate.getAppDelegate().window{
                
                if wn.safeAreaInsets.top > 0{
                    
                    if UIApplication.shared.statusBarFrame.height >= CGFloat(44) {
                        
                        return true
                    }
                }
            }
        }
        
        return false
    }
    
    func playSound(name : String , extn : String) {
        AudioServicesPlayAlertSound(SystemSoundID(1007))
        
//        var soundID: SystemSoundID = 0
//        let mainBundle: CFBundle = CFBundleGetMainBundle()
//        if let ref: CFURL = CFBundleCopyResourceURL(mainBundle, name as CFString, extn as CFString, nil) {
//            
//            AudioServicesCreateSystemSoundID(ref, &soundID)
//            AudioServicesPlaySystemSound(soundID)
//        } else {
//            // print("Could not find sound file")
//        }
    }
    
    func isConnectedToInternet() -> Bool {
        
        let status = Reach().connectionStatus()
        
        if status.description == ReachabilityStatus.online(.wwan).description || status.description == ReachabilityStatus.online(.wiFi).description  // Internet connection available
        {
            return true
        }
        
        return false
    }
    
    func txtleftViewImage(_ textField: UITextField!,_ image: UIImage)
    {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30 , height: 30));
        imageView.image = image;
        let Lview = UIImageView(frame: CGRect(x: 0, y: -5, width: 50 , height: 50));
        Lview.addSubview(imageView)
        imageView.center = Lview.center
        textField.leftView = Lview
        textField.leftViewMode = UITextFieldViewMode.always
        textField.leftViewMode = .always
    }
    
    func showAlertWithTitle(_ strMessage : String, strTitle : String)
    {
        let alert = SCLAlertView()
        alert.showCustom(strTitle, subTitle: strMessage, color: colourTheme, icon: UIImage())
    }
    
    func convertDateToString(_ dateToConvert : Date, format : String) -> String {
        let dateFormatter = DateFormatter()
        
        var paramFormat = format
        
        if isTimeFormatIs12Hour() == false{
            
            paramFormat = format.replacingOccurrences(of: "h", with: "H")
            paramFormat = format.replacingOccurrences(of: "a", with: "")
        }
        
        dateFormatter.dateFormat = paramFormat
        dateFormatter.timeZone = TimeZone.current
        //dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let stringDate = dateFormatter.string(from: dateToConvert)
        return stringDate
    }
    
    func convertStringToDate(_ strToConvert : String, dateFormat : String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = TimeZone.current
       // dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let convertedDate = dateFormatter.date(from: strToConvert)
        return convertedDate!
    }
    
    func isTimeFormatIs12Hour() -> Bool {
        let formatStringForHours = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: NSLocale.current)
        let containsA: NSRange? = (formatStringForHours as NSString?)?.range(of: "a")
        let hasAMPM: Bool = Int(containsA?.location ?? 0) != NSNotFound
        return hasAMPM
    }
    
    //MARK: Date Utility
    func getPostTime(_ postDatetime : Date) -> (String,String) {
        let currentDAte = Date()
        let years = currentDAte.yearsFrom(postDatetime)
        let months = currentDAte.monthsFrom(postDatetime)
        let days = currentDAte.daysFrom(postDatetime)
        let hours = currentDAte.hoursFrom(postDatetime)
        let min = currentDAte.minutesFrom(postDatetime)
        let sec = currentDAte.secondsFrom(postDatetime)
        
        if years != 0 {
            return (String("\(years) year(s) ago"),"year")
            
        }else if months != 0 {
            return (String("\(months) month(s) ago"),"month")
            
        }else if days != 0 {
            return (String("\(days) day(s) ago"),"day")
            
        }else if hours != 0 {
            return (String("\(hours) hour(s) ago"),"hour")
            
        }else if min != 0 && min <= 60 {
            return (String("\(min) min(s) ago"),"min")
            
        }else if sec != 0 && sec <= 60{
            return (String("\(sec) sec(s) ago"),"sec")
            
        }else{
            return (String("just now"),"justnow")
        }
    }
}

extension Date {
    func isBetween(_ date1: Date, and date2: Date) -> Bool {
        return (min(date1, date2) ... max(date1, date2)).contains(self)
    }
}

extension Date {
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
        // or use capitalized(with: locale) if you want
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension String
{
    func replace(target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
}

extension UIColor {
    
    convenience init(hex: Int) {
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
    }
    
}


var key: Void?

class UITextFieldAdditions: NSObject {
    var readonly: Bool = false
}

extension UITextField {
    var readonly: Bool {
        get {
            return self.getAdditions().readonly
        } set {
            self.getAdditions().readonly = newValue
        }
    }
    
    private func getAdditions() -> UITextFieldAdditions {
        var additions = objc_getAssociatedObject(self, &key) as? UITextFieldAdditions
        if additions == nil {
            additions = UITextFieldAdditions()
            objc_setAssociatedObject(self, &key, additions!, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
        return additions!
    }
    
    open override func target(forAction action: Selector, withSender sender: Any?) -> Any? {
        if ((action == #selector(UIResponderStandardEditActions.paste(_:)) || (action == #selector(UIResponderStandardEditActions.cut(_:)))) && self.readonly) {
            return nil
        }
        return super.target(forAction: action, withSender: sender)
    }
}


extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: characters.count)) != nil
    }
}

extension CALayer {
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: self.frame.height - thickness, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
            break
        default:
            break
        }
        border.backgroundColor = color.cgColor;
        self.addSublayer(border)
    }
}
