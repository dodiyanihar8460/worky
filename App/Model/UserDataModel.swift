//
//  userModel.swift
//  HIITList
//
//  Created by Bhavesh Patel on 23/11/17.
//  Copyright © 2018 Birger LLC. All rights reserved.
//

import UIKit

class UserDataModel: NSObject {
    var userId : String = ""
    var userName : String = ""
    var uniqueName :String = ""
    var email : String = ""
    var profilePic : String = ""
    var about : String = ""
    var education : String = ""
    var work : String = ""
    var sports : String = ""
    var hobbies : String = ""
    var moreInfo : String = ""
    var isPrivateAccount : Bool = false

    open func encodeToJSON() -> [String:Any] {
        var nillableDictionary = [String:Any]()
        //nillableDictionary[keyUserId] = self.userId
        nillableDictionary[keyUsername] = self.userName
        nillableDictionary[keyEmail] = self.email
        nillableDictionary[keyProfilePic] = self.profilePic
        nillableDictionary[keyAbout] = self.about
        nillableDictionary[keyEducation] = self.education
        nillableDictionary[keyWork] = self.work
        nillableDictionary[keySports] = self.sports
        nillableDictionary[keyHobbies] = self.hobbies
        nillableDictionary[keyMoreInfo] = self.moreInfo
        nillableDictionary[keyUniqueName] = self.uniqueName
        nillableDictionary[keyisPrivateAccount] = self.isPrivateAccount
        return nillableDictionary
    }

    open func encodeToJSONForSearch() -> [String:Any] {
        var nillableDictionary = [String:Any]()
        nillableDictionary[keyUserId] = self.userId
        nillableDictionary[keyUsername] = self.userName
        nillableDictionary[keyEmail] = self.email
        nillableDictionary[keyProfilePic] = self.profilePic
        nillableDictionary[keyAbout] = self.about
        nillableDictionary[keyEducation] = self.education
        nillableDictionary[keyWork] = self.work
        nillableDictionary[keySports] = self.sports
        nillableDictionary[keyHobbies] = self.hobbies
        nillableDictionary[keyMoreInfo] = self.moreInfo
        nillableDictionary[keyUniqueName] = self.uniqueName
        nillableDictionary[keyisPrivateAccount] = self.isPrivateAccount
        return nillableDictionary
    }
    
    open func decodeJson(_ nillableDictionary :  [String:Any]) -> UserDataModel {
        self.userId = nillableDictionary[keyUserId] as! String
        self.userName = nillableDictionary[keyUsername] as! String
        self.email  = nillableDictionary[keyEmail] as! String
        self.profilePic = nillableDictionary[keyProfilePic] as! String
        self.about = nillableDictionary[keyAbout] as! String
        
        if let value = nillableDictionary[keyEducation] as? String{
            self.education = value
        }
        if let value = nillableDictionary[keyWork] as? String{
            self.work = value
        }
        if let value = nillableDictionary[keySports] as? String{
            self.sports = value
        }
        if let value = nillableDictionary[keyHobbies] as? String{
            self.hobbies = value
        }
        if let value = nillableDictionary[keyMoreInfo] as? String{
            self.moreInfo = value
        }
        if let value = nillableDictionary[keyisPrivateAccount] as? Bool{
            self.isPrivateAccount = value
        }
        
        if let value = nillableDictionary[keyUniqueName] as? String{
            self.uniqueName = value
        }
        
        return self
    }
}
