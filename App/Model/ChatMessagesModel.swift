//
//  ChatMessagesModel.swift
//  HIITList
//
//  Created by Bhavesh Patel on 30/10/17.
//  Copyright © 2018 Birger LLC. All rights reserved.
//

import UIKit
import RNCryptor

class ChatMessagesModel: NSObject {
    var messageId = String()
    var sendby = String()
    var message = String()
    var timeStamp : Double = 0.0
    var isShowUnread = false
    var isImage = false
    //var timeStamp2 : Double = 0.0
    var ViewFlag : Bool = false
    var SendRcvFlag = String()

    func decryptMessage(encryptedMessage: String, encryptionKey: String) throws -> String {
        let encryptedData = Data.init(base64Encoded: encryptedMessage)
        let decryptedData = try RNCryptor.decrypt(data: encryptedData!, withPassword: encryptionKey)
        let decryptedString = String(data: decryptedData, encoding: .utf8)!
        return decryptedString
    }
    
}
