//
//  ChatModel.swift
//  CoolMatch
//
//  Created by Bhavesh Patel on 29/11/16.
//  Copyright © 2016 Bhavesh Patel. All rights reserved.
//

import UIKit

class ChatModel: NSObject {
    var members = NSMutableArray()
    var lastMsgSent = String()
    var timeStamp : Double = 0
}
