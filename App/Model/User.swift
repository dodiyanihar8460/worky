//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import Foundation
import UIKit
import Firebase
import RxSwift
import ObjectMapper

class User: Mappable {
    
    //MARK: Properties
    var name: String?
    var email: String?
    var id: String?
    var profilePic:String = ""
    var uniqueName:String = ""
    var onlineStatus: UserStatus?
    var lastSeen: UserStatus?
    var isPrivateAccount: Bool?
    var conversations: [String: UserConversation]?
    
    static var loginUser : Variable<User?> = Variable(nil)
    
    required init?(map: Map) { }
    
    //MARK: Inits
    init(name: String, email: String, id: String, profilePic: String, isPrivateAccount: Bool = false, uniqueName : String) {
        self.name = name
        self.email = email
        self.id = id
        self.profilePic = profilePic
        self.isPrivateAccount = isPrivateAccount
        self.uniqueName = uniqueName
    }
    
    // Mappable
    func mapping(map: Map) {
        self.name                    <- map["username"]
        self.email                   <- map["email"]
        self.onlineStatus            <- map["onlineStatus"]
        self.lastSeen                <- map["lastSeen"]
        self.isPrivateAccount        <- map["isPrivateAccount"]
        self.conversations        <- map["conversations"]
        self.uniqueName        <- map["uniqueName"]
    }
    
    
    //MARK: Methods
    class func registerUser(withName: String, email: String, password: String, profilePic: String, completion: @escaping (Bool) -> Swift.Void) {
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            if error == nil {
                //user?.sendEmailVerification(completion: nil)
                guard let result = user else {
                    return
                }
                
                let uid = result.user.uid
                
                result.user.sendEmailVerification(completion: { (error) in
                    print(error ?? "")
                })
                let storageRef = Storage.storage().reference().child("usersProfilePics").child((uid))
                let defaultProfilepic = UIImage(named: "ic_defaultProfile")!
                let imageData = UIImageJPEGRepresentation(defaultProfilepic, 0.1)// UIImageJPEGRepresentation(defaultProfilepic, 0.1)
                storageRef.putData(imageData!, metadata: nil, completion: { (metadata, err) in
                    if err == nil {
                        
                        storageRef.downloadURL(completion: { (url, error) in
                            if let path = url?.absoluteString {
                                let values = ["name": withName, "email": email, "profilePicLink": path]
                                Database.database().reference().child("users").child((Auth.auth().currentUser?.uid)!).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                                    if errr == nil {
                                        let userInfo = ["email" : email, "password" : password]
                                        UserDefaults.standard.set(userInfo, forKey: "userInformation")
                                        completion(true)
                                    }
                                })
                                
                            }
                        })
                    }
                })
            }
            else {
                completion(false)
            }
        })
    }
    
    class func loginUser(withEmail: String, password: String, completion: @escaping (Bool) -> Swift.Void) {
        Auth.auth().signIn(withEmail: withEmail, password: password, completion: { (user, error) in
            if error == nil {
                let userInfo = ["email": withEmail, "password": password]
                UserDefaults.standard.set(userInfo, forKey: "userInformation")
                completion(true)
            } else {
                completion(false)
            }
        })
    }
    
    class func logOutUser(completion: @escaping (Bool) -> Swift.Void) {
        do {
            try Auth.auth().signOut()
            UserDefaults.standard.removeObject(forKey: "userInformation")
            completion(true)
        } catch _ {
            completion(false)
        }
    }
    
    class func info(forUserID: String, completion: @escaping (User) -> Swift.Void) {
        Database.database().reference().child("users").child(forUserID).observeSingleEvent(of: .value, with: { (snapshot) in
            if let data = snapshot.value as? [String: Any] {
             
                var name = ""
                if let value  = data["username"] as? String{
                    name = value
                }
                
                var email = ""
                if let value  = data["email"] as? String{
                    email = value
                }
                
                var uniqueName = ""
                if let name = data[""] as? String{
                    
                    uniqueName = name
                }
                
                if let profileUrl = data["profilePic"]  as? String{
                    
                    let user = User.init(name: name, email: email, id: forUserID, profilePic: profileUrl, uniqueName : uniqueName)
                    completion(user)
           
                }else{
                    
                    let user = User.init(name: name, email: email, id: forUserID, profilePic: "", uniqueName : uniqueName)
                    completion(user)
                }
                
//                if let profileUrl = data["profilePic"]  as? String{
//
//                    let link = URL.init(string: profileUrl)
//
//                    URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
//                        if error == nil {
//                            let profilePic = UIImage.init(data: data!)
//                            let user = User.init(name: name, email: email, id: forUserID, profilePic: profilePic!)
//
//                            completion(user)
//                        }
//                    }).resume()
//
//                }else{
//
//                    let user = User.init(name: name, email: email, id: forUserID, profilePic: "UIImage(named: "ic_defaultProfile")!")
//                    completion(user)
//                }
            }
        })
    }
    
//    class func downloadAllUsers(exceptID: String, completion: @escaping (User) -> Swift.Void) {
//        Database.database().reference().child("users").observe(.childAdded, with: { (snapshot) in
//            let id = snapshot.key
//            let data = snapshot.value as! [String: Any]
//            let credentials = data["credentials"] as! [String: String]
//            if id != exceptID {
//                let name = credentials["name"]!
//                let email = credentials["email"]!
//                let link = URL.init(string: credentials["profilePicLink"]!)
//                URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
//                    if error == nil {
//                        let profilePic = UIImage.init(data: data!)
//                        let user = User.init(name: name, email: email, id: id, profilePic: profilePic!)
//                        completion(user)
//                    }
//                }).resume()
//            }
//        })
//    }
    
    class func checkUserVerification(completion: @escaping (Bool) -> Swift.Void) {
        Auth.auth().currentUser?.reload(completion: { (_) in
            let status = (Auth.auth().currentUser?.isEmailVerified)!
            completion(status)
        })
    }
    
    class func observeLoginUser() {
        guard let loginUser = Auth.auth().currentUser else { return }
        // listen for online status
        User.userSessionManager()
        // get login user data
        Database.database().reference()
            .child("users")
            .child(loginUser.uid)
            .observe(.value) { (snapshot) in
                let userObj = Mapper<User>().map(JSONObject: snapshot.value)
                userObj?.id = snapshot.key
                User.loginUser.value = userObj
        }
    }
    
    class func sentFriendshipRequest(userId: String) {
        guard let loginUser = Auth.auth().currentUser else { return }
        Database.database().reference()
            .child("users")
            .child(userId)
            .child("conversations")
            .child(loginUser.uid)
            .updateChildValues(["friendRequestTimestamp": ServerValue.timestamp()]) { (error, ref) in
                print(error?.localizedDescription ?? "No Error")
        }
    }

    
    class func privateAccountToggle(isAllowed: Bool) {
        guard let loginUser = Auth.auth().currentUser else { return }
        Database.database().reference()
            .child("users")
            .child(loginUser.uid)
            .updateChildValues([keyisPrivateAccount: isAllowed]) { (error, ref) in
                print(error?.localizedDescription ?? "No Error")
        }
    }

    class func onlineStatusToggle(isAllowed: Bool) {
        guard let loginUser = Auth.auth().currentUser else { return }
        Database.database().reference()
            .child("users")
            .child(loginUser.uid)
            .child("onlineStatus")
            .updateChildValues(["isAllowed": isAllowed]) { (error, ref) in
                print(error?.localizedDescription ?? "No Error")
        }
    }
    
    class func lastSeenToggle(isAllowed: Bool) {
        guard let loginUser = Auth.auth().currentUser else { return }
        Database.database().reference()
            .child("users")
            .child(loginUser.uid)
            .child("lastSeen")
            .updateChildValues(["isAllowed": isAllowed]) { (error, ref) in
                print(error?.localizedDescription ?? "No Error")
        }
    }
    
    class func userSessionManager() {
        
        guard let loginUser = Auth.auth().currentUser else { return }
        
        // internet connectivity checking reference by firebase
        let connectedRef = Database.database().reference(withPath: ".info/connected")
        connectedRef.observe(.value, with: { snapshot in
            
            // only handle connection established (or I've reconnected after a loss of connection)
            guard let connected = snapshot.value as? Bool, connected else { return }
            
            if currentloggedInUserId != "" && currentloggedInUserId != loginUser.uid{
                return
            }
            
            let db = Database.database().reference()
            let userRef = db.child("users").child(loginUser.uid)
            let onlineStatusRef = userRef.child("onlineStatus")
            let lastSeenRef = userRef.child("lastSeen")
            print("user id = \(loginUser.uid)")
            
            // on disconnect update online status
            onlineStatusRef.onDisconnectUpdateChildValues(
                [
                    "isOnline": false,
                    "timestamp" : ServerValue.timestamp()
                ])
            
            // on disconnect update last seen timestamp
            lastSeenRef.onDisconnectUpdateChildValues(
                [
                    "timestamp" : ServerValue.timestamp()
                ])
            
            // update online status when connected
            onlineStatusRef.updateChildValues(
                [
                    "isOnline": true,
                    "timestamp" : ServerValue.timestamp()
                ])

            // update last seen status when connected
            lastSeenRef.updateChildValues(
                [
                    "timestamp" : ServerValue.timestamp()
                ])
        })
    }
    
    class func userSessionManagerForDisconnect() {
        guard let loginUser = Auth.auth().currentUser else { return }
        
        if currentloggedInUserId != "" && currentloggedInUserId != loginUser.uid{
            return
        }
        let db = Database.database().reference()
        let userRef = db.child("users").child(loginUser.uid)
        let onlineStatusRef = userRef.child("onlineStatus")
        let lastSeenRef = userRef.child("lastSeen")
        
        onlineStatusRef.updateChildValues(["isOnline":false,"timestamp":ServerValue.timestamp()])
        lastSeenRef.updateChildValues(["timestamp":ServerValue.timestamp()])
    }
}

class UserStatus:Mappable {
    var isAllowed: Bool?
    var isOnline: Bool?
    var timestamp: Int?
    
    required init?(map: Map) {}
    
    // Mappable
    func mapping(map: Map) {
        self.isAllowed          <- map["isAllowed"]
        self.isOnline       <- map["isOnline"]
        self.timestamp      <- map["timestamp"]
    }
}

class UserConversation : Mappable {
    
    var location: String?
    var friendRequestTimestamp:Int?
    
    required init?(map: Map) {}
    
    // Mappable
    func mapping(map: Map) {
        self.location          <- map["location"]
        self.friendRequestTimestamp          <- map["friendRequestTimestamp"]
    }
}

