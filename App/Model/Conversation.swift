//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation
import UIKit
import Firebase

class Conversation {
    
    //MARK: Properties
    let user: User
    var lastMessage: Message
    
    //MARK: Methods
    class func showConversations(completion: @escaping ([Conversation]) -> Swift.Void) {
        if let currentUserID = Auth.auth().currentUser?.uid {
            var conversations = [Conversation]()
            Database.database().reference().child("users").child(currentUserID).child("conversations").observe(.childAdded, with: { (snapshot) in
                if snapshot.exists() {
                    let fromID = snapshot.key
                    let values = snapshot.value as? [String: Any] ?? [String:Any]()
                    if let location = values["location"] as? String {
                        User.info(forUserID: fromID, completion: { (user) in
                            let emptyMessage = Message.init(type: .text, content: "", owner: .sender, timestamp: 0, isRead: true, isReceived: true)
                            let conversation = Conversation.init(user: user, lastMessage: emptyMessage)
                            conversations.append(conversation)
                            conversation.lastMessage.downloadLastMessage(forLocation: location, completion: {
                                completion(conversations)
                            })
                        })
                    }else if let requestTimestamp = values["friendRequestTimestamp"] as? Int {
                        User.info(forUserID: fromID, completion: { (user) in
                            let emptyMessage = Message.init(type: .text, content: "Request to start conversation", owner: .sender, timestamp: requestTimestamp/1000, isRead: true, isReceived: true)
                            let conversation = Conversation.init(user: user, lastMessage: emptyMessage)
                            conversations.append(conversation)
                            completion(conversations)
                        })
                    }
                }
            })
        }
    }
    
    
    
    //MARK: Inits
    init(user: User, lastMessage: Message) {
        self.user = user
        self.lastMessage = lastMessage
    }
    
    class func deleteConversation(forUserID: String, forID: String)  {
        
        if let currentUserID = Auth.auth().currentUser?.uid {
            Database.database().reference().child("users").child(currentUserID).child("conversations").child(forUserID).observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as? [String: String] ?? [String:String]()
                    let location = data["location"]!
                    Database.database().reference().child("conversations").child(location).removeValue()
                }
            })
        }
    }
}
