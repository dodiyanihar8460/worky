//
//  AppDelegate.swift
//  App
//
//  Created by Bhavesh patel on 5/27/18.
//  Copyright © 2018 Birger LLC. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift
import UserNotifications
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //register for push notification
        FirebaseApp.configure()
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                // Enable or disable features based on authorization.
            }
            Messaging.messaging().delegate = self
            UIApplication.shared.registerForRemoteNotifications()
        } else {
            let settings = UIUserNotificationSettings(types: [UIUserNotificationType.alert, UIUserNotificationType.sound] , categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
            // Fallback on earlier versions
        }
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 2
        IQKeyboardManager.shared.disabledDistanceHandlingClasses = [ChattingVC.self]
       // IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(YourViewController.self)
        
        let userDefaults = UserDefaults.standard
        if userDefaults.value(forKey: keyAppFirstTimeOpen) == nil {
            //if app is first time opened then it will be nil
            userDefaults.setValue(true, forKey: keyAppFirstTimeOpen)
            // signOut from FIRAuth
            do {
                try Auth.auth().signOut()
            }catch {
                
            }
            // go to beginning of app
        } else {
            //go to where you want
        }
        
        if let _ = Auth.auth().currentUser {
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            
            let initialViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HometabbarVC")
            appDelegate.window?.rootViewController = initialViewController
            appDelegate.window?.makeKeyAndVisible()
        }
        
        Database.database().isPersistenceEnabled = true
        
        return true
    }
    
    override init() {
        super.init()
       // FirebaseApp.configure()
        // not really needed unless you really need it FIRDatabase.database().persistenceEnabled = true
    }

    
//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                willPresent notification: UNNotification,
//                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
//    {
//        UIApplication.shared.applicationIconBadgeNumber = 0
//        if let uName =  userDefaults.value(forKey: "isChattingwith") as? String
//        {
//            let title = notification.request.content.title
//
//            if uName == title {
//                completionHandler(.sound)
//            }else{
//                completionHandler([.alert,.sound,.badge])
//            }
//        }else{
//            completionHandler([.alert,.sound,.badge])
//        }
//    }
   
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification
        userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("APN recieved")
        // print(userInfo)
        application.applicationIconBadgeNumber = 0
        let state = application.applicationState
        switch state {
            
        case .inactive:
            print("Inactive")
        case .background:
            print("Background")
            
        case .active:
            print("Active")
            
            self.notify_eventReceived(userInfo)
            
            application.applicationIconBadgeNumber = 0
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                UserDefaults.standard.set(result.token, forKey: keyDeviceToken)
                UserDefaults.standard.synchronize()
            }
        })
    }
   
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        print("didReceiveRemoteNotification Userinfo %@",userInfo)
//        let dict : NSDictionary = userInfo as NSDictionary
//        if let notification:NSDictionary = dict.object(forKey: "aps") as? NSDictionary
//        {
//            //            if let message:NSString = notification.object(forKey: "alert") as? NSString
//            //            {
//            //
//            //            }
//        }
//    }
//    
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func application( _ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data )
    {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        //        UserDefaults.standard.set(deviceTokenString, forKey: keyDeviceToken)
        //        UserDefaults.standard.synchronize()
        print("Device Token :\(deviceTokenString)")
        //Messaging.messaging().apnsToken = deviceToken
        Messaging.messaging().setAPNSToken(deviceToken, type: MessagingAPNSTokenType.sandbox)
        
    }
    
    func application( _ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error )
    {
        print("Notification Error:%@", error.localizedDescription)
    }

    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {
     
        User.userSessionManagerForDisconnect()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        User.userSessionManager()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func notify_eventReceived(_ data : [AnyHashable: Any]){
        print(data)
        
        let appCurrentState = UIApplication.shared.applicationState
        
        if appCurrentState == .active{
            
            
            var message = "New event "
            var title = ""
            
            let img = UIImage(named: "ic_logo")
            
            if let dict = data["aps"] as? [String: Any]{
            
                if let dictInfo = dict["alert"] as? [String:Any]{
                
                    if let val = dictInfo["title"] as? String {
                        
                        if self.showChatNotificationInForeground(val) == false {
                            
                            return
                        }
                        
                        title = val
                    }
                    
                    if let val = dictInfo["body"] as? String {
                        
                        message = val
                    }
                }
            }
            
            let banner = Banner(title: title, subtitle: message, image:img)
            // , backgroundColor: Constant.Colors.APP_LIGHT_GRAY
            
            banner.dismissesOnTap = true
            banner.dictData =  data
            
            banner.setCompletionHandlerDidTap({ (myDictdata ) in
                
                self.notifcationTap(title)
            })
            banner.show(duration: 5.0)
            
        }else if appCurrentState == .inactive{
            
        }
        
    }
    
    func notifcationTap(_ title : String ){
        
//        guard let uid = Auth.auth().currentUser?.uid else{
//            return
//        }
//        Database.database().reference().child(nodeUserChats).child(uid).observe(.value, with: { (snapshot) in
//            if !(snapshot.exists()) {
//                
//                return
//            }
//            var chatIds = NSArray()
//            if let defaults = (snapshot.value as! NSDictionary)[keyChatID] as? NSArray {
//                chatIds = defaults
//            }
//            self.mainArray.removeAllObjects()
//            for i  in chatIds{
//                let oponent_id = ((i as! String).replace(target: uid, withString: "")).replace(target: "-", withString: "")
//                let model = conversationModel()
//                model.chatID  = "\(i)"
//                let mygroup = DispatchGroup()
//                mygroup.enter()
//                let _ = self.ref.child(nodeChatMessages).child(model.chatID).queryLimited(toLast: 1).observeSingleEvent(of: .childAdded, with: { (snapshot) in
//                    
//                    if let defaults = (snapshot.value as! NSDictionary)[keyMessage] as? String {
//                        model.lastMessage = defaults
//                    }
//                    
//                    if let defaults = (snapshot.value as! NSDictionary)[keyTimeStamp] as? NSNumber {
//                        model.time = defaults
//                    }
//                    mygroup.leave()
//                    // self.tblView.reloadData()
//                })
//                mygroup.notify(queue: DispatchQueue.global(), execute: {
//                    let _ = self.ref.child(nodeChatMessages).child(i as! String).queryOrdered(byChild: keyViewFlag).queryEqual(toValue: false).observe(.value, with: { snapshot2 in
//                        var c = 0
//                        for j in snapshot2.children {
//                            if let defaults = ((j as! DataSnapshot).value as! NSDictionary)[keySentBy] as? String {
//                                if(defaults == oponent_id)
//                                {
//                                    c += 1
//                                }
//                            }
//                            
//                            if let defaults = ((j as! DataSnapshot).value as! NSDictionary)[keyMessage] as? String {
//                                model.lastMessage = defaults
//                            }
//                            
//                            if let defaults = ((j as! DataSnapshot).value as! NSDictionary)[keyTimeStamp] as? NSNumber {
//                                model.time = defaults
//                            }
//                        }
//                        
//                        if let foo = self.mainArray.first(where: {($0 as! conversationModel).UserId == oponent_id}) {
//                            (foo as! conversationModel).Unread = "\(c)"
//                            
//                            if let topController = self.navigationController?.visibleViewController {
//                                
//                                if (topController.classForCoder == ConversationsVC.self) {
//                                    if self.mainArray.count > 0 {
//                                        if let arr = (self.mainArray.sorted { ($0 as! conversationModel).time.intValue > ($1 as! conversationModel).time.intValue } as? NSArray)
//                                        {
//                                            self.mainArray = arr.mutableCopy() as! NSMutableArray
//                                        }
//                                        self.tblView.reloadData()
//                                    }
//                                }
//                            }
//                            
//                            self.tblView.reloadData()
//                        } else {
//                            model.Unread = "\(c)"
//                            
//                            if let topController = self.navigationController?.visibleViewController {
//                                
//                                if (topController.classForCoder == ConversationsVC.self) {
//                                    if self.mainArray.count > 0 {
//                                        if let arr = (self.mainArray.sorted { ($0 as! conversationModel).time.intValue > ($1 as! conversationModel).time.intValue } as? NSArray)
//                                        {
//                                            self.mainArray = arr.mutableCopy() as! NSMutableArray
//                                        }
//                                        self.tblView.reloadData()
//                                    }
//                                }
//                            }
//                        }
//                    })
//                })
//                model.UserId = oponent_id
//                let _ = self.ref.child(nodeUsers).queryOrderedByKey().queryEqual(toValue: oponent_id).observe(.childAdded, with: { snapshot in
//                    if !snapshot.exists() {return}
//                    var name = ""
//                    
//                    if let defaults = (snapshot.value as! NSDictionary)[keyUsername] as? String {
//                        name =  name + defaults
//                    }
//                    
//                    if let defaults = (snapshot.value as! NSDictionary)[keyProfilePic] as? String {
//                        model.profilePic = defaults
//                    }
//
//                    model.UserName = name
//                    self.mainArray.add(model)
//                    if let topController = self.navigationController?.visibleViewController {
//                        
//                        if (topController.classForCoder == ConversationsVC.self) {
//                            if self.mainArray.count > 0 {
//                                if let arr = (self.mainArray.sorted { ($0 as! conversationModel).time.intValue > ($1 as! conversationModel).time.intValue } as? NSArray)
//                                {
//                                    self.mainArray = arr.mutableCopy() as! NSMutableArray
//                                }
//                            }
//                        }
//                    }
//                    
//                })
//            }
//        })
    }
    
    func showChatNotificationInForeground(_ title : String) -> Bool{
        
        if let uName =  userDefaults.value(forKey: keyIsChattingwith) as? String
        {
            if uName == title {
                Utills.instance.playSound(name: "", extn: "wav")
            }else{

                return true
            }
        }else{
            return true
        }
        
        return false
    }
}


extension AppDelegate {
    
    class func getAppDelegate() -> AppDelegate {
        
        return UIApplication.shared.delegate as! AppDelegate
    }
}
