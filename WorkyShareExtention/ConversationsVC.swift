//
//  ConversationsVC.swift
//  HIITList
//
//  Created by Bhavesh Patel on 06/11/17.
//  Copyright © 2018 Birger LLC. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseDatabase
//import SDWebImage

class ConversationsVC: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    
    var items = [Conversation]()
    var selectedUser: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FirebaseApp.configure()
        self.customization()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        if let uName =  userDefaults.value(forKey: keyUserId) as? String{
            
            print(uname)
        }
        self.fetchData()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        if let selectionIndexPath = self.tblView.indexPathForSelectedRow {
            self.tblView.deselectRow(at: selectionIndexPath, animated: animated)
        }
        userDefaults.set("", forKey: keyIsChattingwith)
        userDefaults.set("", forKey: keyCurrentChatUserId)
    }
    
    func customization()  {
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushToUserMesssages(notification:)), name: NSNotification.Name(rawValue: "showUserMessages"), object: nil)
        tblView.register(UINib(nibName: "MessagesTableViewCell", bundle: nil), forCellReuseIdentifier: "MessagesCell")
        tblView.delegate = self
        tblView.dataSource = self
        tblView.tableFooterView = UIView.init(frame: CGRect.zero)
    }
    
    //Downloads conversations
    func fetchData() {
        Conversation.showConversations { (conversations) in
            self.items = conversations
            self.items.sort{ $0.lastMessage.timestamp > $1.lastMessage.timestamp }
            DispatchQueue.main.async {
                self.tblView.reloadData()
            }
        }
    }
    
    //Shows profile extra view
    @objc func showProfile() {
        let info = ["viewType" : ShowExtraView.profile]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showExtraView"), object: nil, userInfo: info)
        self.inputView?.isHidden = true
    }
    
    //Shows contacts extra view
    @objc func showContacts() {
        let info = ["viewType" : ShowExtraView.contacts]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showExtraView"), object: nil, userInfo: info)
    }
    
    //Shows Chat viewcontroller with given user
    @objc func pushToUserMesssages(notification: NSNotification) {
        if let user = notification.userInfo?["user"] as? User {
            self.selectedUser = user
            self.performSegue(withIdentifier: "segue", sender: self)
        }
    }
}

extension  ConversationsVC : UITableViewDelegate, UITableViewDataSource
{
    //MARK: UITableView Delegate & Data Source
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        if self.items.count == 0 {
        //            return 1
        //        } else {
        return self.items.count
        //        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.items.count {
        case 0:
            return UITableViewCell()
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessagesCell", for: indexPath) as! MessagesTableViewCell
            
            cell.imgUser.image = #imageLiteral(resourceName: "ic_defaultProfile")
//            cell.imgUser.sd_setShowActivityIndicatorView(true)
//            cell.imgUser.sd_setIndicatorStyle(.gray)
//            cell.imgUser.sd_setImage(with: URL(string: self.items[indexPath.row].user.profilePic), completed: { (image, error, cacheType, url) in
//                if (error != nil) {
//                    cell.imgUser.image = #imageLiteral(resourceName: "ic_defaultProfile")
//                }
//            })
            
            // cell.imgUser.image = self.items[indexPath.row].user.profilePic
            cell.lblName.text = self.items[indexPath.row].user.name
            
            switch self.items[indexPath.row].lastMessage.type {
            case .text:
                let message = self.items[indexPath.row].lastMessage.content as! String
                cell.lblLastMessage.text = message
                
            case .location:
                cell.lblLastMessage.text = "Location"
            default:
                cell.lblLastMessage.text = "Media"
            }
            
            if self.items[indexPath.row].lastMessage.owner == .sender{
                
                if self.items[indexPath.row].lastMessage.totalUnreadCount > 0{
                    
                    cell.badge.layer.cornerRadius = cell.badge.frame.height / 2
                    cell.badge.isHidden = false
                    cell.badge.text = "\(self.items[indexPath.row].lastMessage.totalUnreadCount)"
                    
                }else{
                    
                    cell.badge.isHidden = true
                }
                
            }else{
                cell.badge.isHidden = true
            }
            
            let messageDate = Date.init(timeIntervalSince1970: TimeInterval(self.items[indexPath.row].lastMessage.timestamp))
            //let dataformatter = DateFormatter.init()
            //dataformatter.timeStyle = .short
            //let date = dataformatter.string(from: messageDate)
            
            if self.items[indexPath.row].lastMessage.timestamp != 0{
                
                let calendar = NSCalendar.current
                
                if calendar.isDateInToday(Date(timeIntervalSince1970: TimeInterval(self.items[indexPath.row].lastMessage.timestamp))) {
                    
                    cell.lblTime.text = Utility_Extention.instance.convertDateToString(messageDate as Date, format: "hh:mm a")
                    
                }else if calendar.isDateInYesterday(Date(timeIntervalSince1970: TimeInterval(self.items[indexPath.row].lastMessage.timestamp))){
                    
                    cell.lblTime.text = "Yesterday"
                    
                }else{
                    
                    cell.lblTime.text = Utility_Extention.instance.convertDateToString(messageDate as Date, format: "MMM dd, yyyy")
                }
                
                //cell.lblTime.text = date
            }else{
                
                cell.lblTime.text = ""
            }
            
            if self.items[indexPath.row].lastMessage.owner == .sender && self.items[indexPath.row].lastMessage.isRead == false {
                //                cell.nameLabel.font = UIFont(name:"AvenirNext-DemiBold", size: 17.0)
                //                cell.messageLabel.font = UIFont(name:"AvenirNext-DemiBold", size: 14.0)
                //                cell.timeLabel.font = UIFont(name:"AvenirNext-DemiBold", size: 13.0)
                //                cell.profilePic.layer.borderColor =
                //                cell.messageLabel.textColor = GlobalVariables.purple
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Clear Chat"
    }
    
    private func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            guard self.items.count > indexPath.row else{
                
                return
            }
            
            let chatObj = self.items[indexPath.row]
            
            Conversation.deleteConversation(forUserID: chatObj.user.id!, forID: "")
            
            DispatchQueue.main.async {
                
                self.fetchData()
            }
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
}
